#pragma once
#include <iostream>

class Base
{
public:
	virtual void show() const = 0;
	virtual ~Base() {}
};

class Derv1 : public Base
{
public:
	void show() const
	{
		std::cout << "Hello!" << std::endl;
	}

};

class Derv2 : public Base
{
public:
	void show() const
	{
		std::cout << "Bye!" << std::endl;
	}
};

class Print
{
protected:
	int a;
public:
	Print(int i) : a(i)
	{}
	virtual void print() const = 0;
};

class PrintInt : public Print
{
public:
	PrintInt(int i) : Print(i)
	{}
	void print() const
	{
		std::cout << a << std::endl;
	}
};

class Print2Int : public Print
{
public:
	Print2Int(int i) : Print(i)
	{}
	void print() const
	{
		std::cout << 2 * a << std::endl;
	}
};
