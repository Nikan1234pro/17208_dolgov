#include "factory.h"
#include "classes.h"
#include <string>

int main()
{
	Factory<std::string, Base> *factory1 = Factory<std::string, Base>::GetInstance();
	factory1->Add<Derv1>("Derv1");
	factory1->Add<Derv2>("Derv2");

	Base *p[2];
	p[0] = factory1->create("Derv1");
	p[1] = factory1->create("Derv2");

	p[0]->show();
	p[1]->show();


	Factory<char, Print, int> *factory2 = Factory<char, Print, int>::GetInstance();
	factory2->Add<PrintInt>('a');
	factory2->Add<Print2Int>('b');

	Print *k[2];
	k[0] = factory2->create('a', 6);
	k[1] = factory2->create('b', 7);

	k[0]->print();
	k[1]->print();

	system("pause");
	return 0;
}