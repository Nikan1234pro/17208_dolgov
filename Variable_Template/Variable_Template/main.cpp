#include <ostream>
#include <tuple>
#include <iostream>
#include <string>

namespace tpl
{
	template <typename Stream, typename Type, size_t _Cur, size_t _Last>
	class tuple_printer
	{
	public:
		static void print(Stream& os, const Type &t) 
		{
			os << std::get<_Cur>(t) << ", ";
			tuple_printer<Stream, Type, _Cur + 1, _Last>::print(os, t);
		}
	};

	template <typename Stream, typename Type, size_t _Last>
	class tuple_printer<Stream, Type, _Last, _Last>
	{
	public:
		static void print(Stream& os, const Type &t)
		{
			os << std::get<_Last>(t);
		}
	};	
}

template <typename Ch, typename Tr, typename... Args>
auto operator<<(std::basic_ostream<Ch, Tr> &os, const std::tuple<Args...> &t)
->std::basic_ostream<Ch, Tr>&
{
	os << '(';
	tpl::tuple_printer<std::basic_ostream<Ch, Tr>, std::tuple<Args...>, 0, sizeof...(Args) - 1>::print(os, t);
	os << ')' << std::endl;
	return os;
}


int main()
{
	std::tuple<int, char, double, std::string> t(10, 's', 12.567, "Nikita");
	
	std::cout << t << std::endl;
	system("pause");
	return 0;
}