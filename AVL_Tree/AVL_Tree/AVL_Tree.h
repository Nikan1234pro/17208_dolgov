#pragma once
#define MAX(a, b) ((a) > (b)) ? (a) : (b)
template <typename T>
class AVL_Tree
{
private:
	struct Node
	{
		T data;
		int height;
		Node *left;
		Node *right;
		Node(const T& _data) : data(_data), height(1), 
			left(nullptr), right(nullptr)
		{}

	};
	Node *Add(const T& data, Node *ptr);
	Node* Balance(Node *ptr);
	Node* Rotation_Left(Node *ptr);
	Node* Rotation_Right(Node *ptr);
	int Balance_Factor(Node *ptr);
	void Recovery_Height(Node *ptr);
	void Clear(Node *ptr);
public:
	Node *Head;
	AVL_Tree();
	void Add(const T& data);
	void Find(T &data);
	void Print(Node *ptr);
	~AVL_Tree();
};

template <typename T>
AVL_Tree<T>::AVL_Tree() : Head(nullptr)
{}

template<typename T>
int AVL_Tree<T>::Balance_Factor(AVL_Tree<T>::Node *ptr)
{
	int height_L = ((ptr->left) ? ptr->left->height : 0);
	int height_R = ((ptr->right) ? ptr->right->height : 0);
	return height_R - height_L;
}

template <typename T>
void AVL_Tree<T>::Recovery_Height(AVL_Tree<T>::Node* ptr)
{
	int height_L = ((ptr->left) ? ptr->left->height : 0);
	int height_R = ((ptr->right) ? ptr->right->height : 0);
	ptr->height = MAX(height_L, height_R) + 1;
}

template <typename T>
typename AVL_Tree<T>::Node* AVL_Tree<T>::Rotation_Left(AVL_Tree<T>::Node *ptr)
{
	Node *temp;
	temp = ptr->right;
	ptr->right = temp->left;
	temp->left = ptr;
	
	Recovery_Height(ptr);
	Recovery_Height(temp);
	return temp;
}

template <typename T>
typename AVL_Tree<T>::Node* AVL_Tree<T>::Rotation_Right(AVL_Tree<T>::Node *ptr)
{
	Node *temp;
	temp = ptr->left;
	ptr->left = temp->right;
	temp->right = ptr;
	
	Recovery_Height(ptr);
	Recovery_Height(temp);
	return temp;
}

template <typename T>
typename AVL_Tree<T>::Node* AVL_Tree<T>::Balance(AVL_Tree<T>::Node *ptr)
{
	Recovery_Height(ptr);
	if (Balance_Factor(ptr) == 2)
	{
		if (Balance_Factor(ptr->right) < 0)
			ptr->right = Rotation_Right(ptr->right);
		return Rotation_Left(ptr);
	}
	if (Balance_Factor(ptr) == -2)
	{
		if (Balance_Factor(ptr->left) > 0)
			ptr->left = Rotation_Left(ptr->left);
		return Rotation_Right(ptr);
	}
	return ptr;
}

template <typename T>
void AVL_Tree<T>::Add(const T& data)
{
	if (Head == nullptr)
		Head = new Node(data);
	else
		Head = Add(data, Head);
}

template <typename T>
typename AVL_Tree<T>::Node *AVL_Tree<T>::Add(const T& data, AVL_Tree<T>::Node *ptr)
{
	if (ptr == nullptr)
	{
		ptr = new Node(data);
		return ptr;
	}
	if (data > ptr->data)
		ptr->left = Add(data, ptr->left);
	else
		ptr->right = Add(data, ptr->right);
	ptr = Balance(ptr);
	return ptr;
}

template <typename T>
void AVL_Tree<T>::Clear(AVL_Tree<T>::Node *ptr)
{
	if (ptr == nullptr)
		return;
	Clear(ptr->left);
	Clear(ptr->right);
	delete ptr;
}

template <typename T>
AVL_Tree<T>::~AVL_Tree()
{
	Clear(Head);
}

template<typename T>
void AVL_Tree<T>::Print(AVL_Tree<T>::Node *ptr)
{
	if (ptr == nullptr)
		return;
	std::cout << ptr->data << "   ";
	Print(ptr->left);
	Print(ptr->right);
}






