#include <iostream>
#include "AVL_Tree.h"

int main()
{
	AVL_Tree<int> *tree = new AVL_Tree<int>;
	tree->Add(10);
	tree->Add(5);
	tree->Add(100);
	tree->Print(tree->Head);
	delete tree;
	std::cin.get();
	return 0;
}