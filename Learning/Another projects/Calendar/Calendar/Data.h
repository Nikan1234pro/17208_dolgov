#pragma once
const int clk_60 = 60;
const int clk_24 = 24;
const int clk_12 = 12;
enum Months { Jan = 1, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec };

class Data
{
private:
	
	int sec;
	int min;
	int hour;
	int day;
	Months month;
	int year;
	Months get_month(int) const;
	int get_day(int, int) const;
public:
	Data();
	Data(int y, Months m, int d, int h, int mi, int s);
	Data(unsigned int y, Months m, unsigned int d);
	Data(unsigned int h, unsigned int mi, unsigned int d);
	Data addSeconds(int);
	Data addMinutes(int);
	Data addHours(int);
	Data addDays(int);
	Data addMonths(int);
	Data addYears(int);
	void Show() const;
};