#include <iostream>
#include "Data.h"

int main()
{
	Data a(2010, Jan, 1, 0, 0, 12);
	a.Show();
	Data b = a.addSeconds(-14);
	b.Show();
	b = b.addMonths(2);
	b.Show();
	b = b.addMinutes(-60);
	b.Show();
	b = b.addYears(12);
	b = b.addMonths(12);
	b.Show();
	system("pause");
	return 0;
}