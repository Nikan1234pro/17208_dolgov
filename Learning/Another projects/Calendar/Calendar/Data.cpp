#include <iostream>
#include <process.h>
#include <time.h>
#include "Data.h"

Months Data::get_month(int month) const
{
	int n = (month - 1) % clk_12;
	switch (n)
	{
	case 0:	 return Jan;
	case 1:	 return Feb;
	case 2:	 return Mar;
	case 3:	 return Apr;
	case 4:	 return May;
	case 5:	 return Jun;
	case 6:	 return Jul;
	case 7:	 return Aug;
	case 8:  return Sep;
	case 9: return Oct;
	case 10: return Nov;
	case 11: return Dec;
	}
}

int Data::get_day(int m, int year) const
{
	if (!m)
		m = clk_12 - m;

	//std::cout << "need = " << m << std::endl;
	static const int Days[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	if (((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) && m == Feb)	//���� ���������� ��� � �������
	{
		//std::cout << "HUI" << std::endl;
		return Days[Feb - 1] + 1;
	}
	else
	{
		return Days[m - 1];
	}
		
}

Data::Data()
{
	errno_t err;
	tm now;
	time_t t = time(NULL);
	err = localtime_s(&now, &t);
	if (err)
	{
		std::cout << "Error!" << std::endl;
		exit(1);
	}
	year = now.tm_year + 1900;
	month = get_month(now.tm_mon + 1);
	day = now.tm_mday;
	hour = now.tm_hour;
	min = now.tm_min;
	sec = now.tm_sec;
}

Data::Data(int old_year, Months old_mon, int old_day, int old_hour, int old_min, int old_sec)
{
	//������� ������
	sec = old_sec % clk_60;
	if (sec < 0)
		sec = sec + clk_60;
	old_min += (old_sec - sec) / clk_60;
	//������� �����
	min = old_min % clk_60;
	if (min < 0)
		min = min + clk_60;
	old_hour += (old_min - min) / clk_60;
	//������� �����
	hour = old_hour % clk_24;
	if (hour < 0)
		hour = hour + clk_24;
	old_day += (old_hour - hour) / clk_24;
	//������� ���, ������, ����
	int new_mon = ((old_mon - 1) % clk_12) + 1;
	if (new_mon <= 0)
		new_mon += clk_12;
	old_year += old_mon / clk_12;

	if (old_day > 0)
	{
		while (old_day > get_day(new_mon, old_year))
		{
			old_day -= get_day(new_mon, old_year);
			new_mon++;
			if (new_mon > 12)
			{
				new_mon %= clk_12;
				old_year++;
			}
		}
	}
	else
	{
		while (old_day <= 0)
		{
			old_day += get_day(new_mon - 1, old_year);
			new_mon--;
			if (!new_mon)
			{
				old_year--;
				new_mon = Dec;
			}
		}
	}
	day = old_day;
	month = get_month(new_mon);
	year = old_year;
}

void Data::Show() const
{
	using namespace std;
	cout << year << '-';
	cout << month << '-';
	cout << day << ' ';
	cout << hour << ':';
	cout << min << ':';
	cout << sec << endl;
}

Data Data::addSeconds(int n)
{
	return Data(year, month, day, hour, min, sec + n);
}

Data Data::addMinutes(int n)
{
	return Data(year, month, day, hour, min + n, sec);
}

Data Data::addHours(int n)
{
	return Data(year, month, day, hour + n, min, sec);
}

Data Data::addDays(int n)
{
	return Data(year, month, day + n, hour, min, sec);
}

Data Data::addMonths(int n)
{
	return Data(year, static_cast<Months>(month + n), day, hour, min, sec);
}

Data Data::addYears(int n)
{
	return Data(year + n, month, day, hour, min, sec);
}

