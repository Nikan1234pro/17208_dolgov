#include <iostream>
#include <fstream>
const int MAX = 100;
using namespace std;
int main()
{
	ofstream file_out("test.txt", ios::binary);
	if (!file_out)
	{
		cerr << "File coundn't open!" << endl;
		return 1;
	}
		
	int buf[MAX];
	for (int i = 0; i < MAX; i++)
		buf[i] = i * 10;

	file_out.write(reinterpret_cast<char*>(buf), sizeof(int) * MAX);
	if (!file_out)
	{
		cerr << "Error!" << endl;
		return 1;
	}
		
	file_out.close();
	for (int i = 0; i < MAX; i++)
		buf[i] = 0;

	ifstream file_in("test.txt", ios::binary);
	//file_in.seekg(sizeof(int), ios::beg);
	file_in.read(reinterpret_cast<char*>(buf), sizeof(int) * MAX);
	if (!file_in)
	{
		cerr << "Read failed!" << endl;
		return 1;
	}
		
	for (int i = 0; i < MAX; i++)
	{
		cout << i * 10 << " - " << buf[i] << endl;
		if (buf[i] != i * 10)
			cerr << "Data doesn't match!" << endl;
	}
		
	cout << "All completed!" << endl;
	system("pause");
	return 0;
}