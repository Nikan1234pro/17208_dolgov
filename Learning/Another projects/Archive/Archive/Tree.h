#pragma once
struct Tree;

void Create_Queue(Tree *Head, const unsigned int *Table);
void Create_Tree(Tree *Head);
void Save_Tree(const Tree *Head, unsigned char *buf, unsigned char &code, char &position, short int &size_tree);
Tree* Re_Create_Tree(std::fstream &file_in, unsigned char *buf, char &position, int &point, short int size_tree);
Tree *init_Tree();

void Obhod(Tree *Head);
