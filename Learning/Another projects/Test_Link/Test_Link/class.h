#pragma once
#define parameter 0

#include <iostream>
class Data
{
private:
	int value;
	static int count;
	char ch;
public:
	Data() : value(0), ch('a')
	{
		std::cout << "Запущен 1 конструктор!" << std::endl;
		count++;
	}

	Data(int f) : value(f), ch('b')
	{
		std::cout << "Запущен 2 конструктор!" << std::endl;
		count++;
	}

	Data(const Data& a)
	{
		value = a.value;
		ch = a.ch;
		std::cout << "Запущен конструктор копирования!" << std::endl;
		count++;
	}

	~Data()
	{
		//count--;
	}

	int get_data() const
	{
		return value;
	}

#if parameter
	Data operator=(const Data &D)
	{
		std::cout << "(!!!)" << std::endl;
		value = D.value;
		return Data(value);
	}
#else
	Data& operator=(const Data &D)
	{
		std::cout << "(!!!)" << std::endl;
		value = D.value;
		return *this;
	}
#endif // parameter

	static void get_count() 
	{
		std::cout << "Count = " << count << std::endl;
	}

};

int Data::count = 0;