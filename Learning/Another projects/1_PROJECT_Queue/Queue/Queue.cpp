#include "Queue.h"

template <typename T>
Queue<T>::node::node(T value) : data(value), next(NULL)
{}

template <typename T>
Queue<T>::node::~node()
{
	std::cout << "List with value " << data << " was deleted!" << std::endl;
}

template <typename T>
Queue<T>::Queue() : count(0), back(NULL), front(NULL)
{}

template <typename T>
Queue<T>::~Queue()
{
	while (front)
	{
		node *temp = front;
		front = front->next;
		delete temp;
	}
}

template <typename T>
void Queue<T>::push_back(T value)
{
	node *temp = new node(value);
	if (!back && !front)
		back = front = temp;
	else
	{
		back->next = temp;
		back = temp;
	}
	count++;
}

template <typename T>
T Queue<T>::pop_front()
{
	if (!front)
	{
		std::cout << "Queue is empty!" << std::endl;
		return -1;
	}
	node *temp = front;
	front = front->next;
	T value = temp->data;
	delete temp;
	count--;
	return value;
}

template <typename T>
void Queue<T>::show() const
{
	for (node *temp = front; temp != NULL; temp = temp->next)
		std::cout << temp->data << " ";
	std::cout << std::endl;
}

template <typename T>
int Queue<T>::get_count() 
{
	return count;
}