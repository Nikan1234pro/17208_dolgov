#include "Queue.cpp"
using namespace std;
int main()
{
	Queue<int> *Q = new Queue<int>;
	Q->push_back(10);
	Q->push_back(23);
	Q->push_back(37);
	Q->push_back(56);
	Q->push_back(32);
	Q->push_back(118);

	Q->show();

	Q->pop_front();
	Q->pop_front();

	Q->show();

	Q->push_back(333);
	Q->show();

	delete Q;
	system("pause");
	return 0;
}