#pragma once
#include <iostream>
template <typename T>
class Queue
{
private:
	class node
	{
		friend class Queue<T>;
	private:
		T data;
		node *next;
		node(T value);
		~node();
	};
	node *front;
	node *back;
	int count;
public:
	Queue();
	~Queue();
	void push_back(T);
	T pop_front();
	void show() const;
	int get_count();
};

