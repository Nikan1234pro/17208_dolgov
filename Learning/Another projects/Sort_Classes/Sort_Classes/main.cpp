#include "class.h"

void my_swap(person **x, person **y)
{
	if ((*x)->get() > (*y)->get())
	{
		person *temp = *x;
		*x = *y;
		*y = temp;
	}
}

void bsort(person **arr, int n)
{
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			my_swap(arr + i, arr + j);
}

int main()
{
	person *arr[100];
	int n = 0;
	char ch;
	do
	{
		arr[n] = new person;
		arr[n]->set();
		n++;
		cout << "Continue? (y / n): ";
		cin >> ch;
	} while (ch != 'n');

	bsort(arr, n);

	for (int i = 0; i < n; i++)
		arr[i]->show();
	system("pause");
	return 0;
}