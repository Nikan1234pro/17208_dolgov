#pragma once
#include <fstream>
#include <iostream>
#define INFINITY INT_MAX + 2
#define WHITE 0
#define RED 1
class Dijkstra
{
private:
	unsigned int N;
	unsigned int M;
	unsigned int Start;
	unsigned int Finish;
	unsigned int **Matrix;
	unsigned int *min_weight;
	unsigned int *path;
	bool *color;

	unsigned int Print_Min_Path(std::ostream&) const;
	void Find_Path(int, std::ostream&) const;
public:
	Dijkstra();
	Dijkstra(std::ifstream&);
	~Dijkstra();
	void Create_Matrix(std::ifstream&);
	void showMatrix() const;
	void Minimal_Distance(std::ostream&);


};