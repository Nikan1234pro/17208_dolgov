#include "Class_1.h"
#include <process.h>
#include <iomanip>

Dijkstra::Dijkstra() : N(0), M(0), Start(0), Finish(0), Matrix(NULL)
{}
Dijkstra::Dijkstra(std::ifstream& file)
{
	Matrix = NULL;
	N = 0;
	M = 0;
	Start = 0;
	Finish = 0;
	file >> N >> Start >> Finish >> M;

	if (file.fail())
		throw "bad number of lines";
	else if ((M < 0 || M > N*(N - 1) / 2))
		throw "bad number of edges";
	else if (Start <= 0 || Start > N || Finish <= 0 || Finish > N)
		throw "bad vertex";
	else if ((N < 0 || N > 5000))
		throw "bad number of vertices";
}
Dijkstra::~Dijkstra()
{
	delete[] color;
	delete[] path;
	delete[] min_weight;

	for (int i = 0; i < N; i++)
		delete[] Matrix[i];
	delete[] Matrix;
}
void Dijkstra::showMatrix() const
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
			std::cout << std::setw(2) 
			<< std::setiosflags(std::ios::left) 
			<< Matrix[i][j];
		std::cout << std::endl;
	}
}
void Dijkstra::Create_Matrix(std::ifstream &file)
{
	unsigned int vertex1 = 0, vertex2 = 0, edge = 0;
	Matrix = new unsigned int*[N];
	if (!Matrix)
		throw "memory error";
	for (int i = 0; i < N; i++)
	{
		Matrix[i] = new unsigned int[N];
		if (!Matrix[i])
			throw "memory error";
	}
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			Matrix[i][j] = 0;
	for (int i = 0; i < M; i++)
	{
		file >> vertex1 >> vertex2 >> edge;
		if (file.fail())
		{
			std::cout << vertex1 << ' ' << vertex2 << ' ' << edge << std::endl;
			throw "bad number of lines";
		}
			
		else if (vertex1 > N || vertex2 > N || vertex1 <= 0 || vertex2 <= 0)
			throw "bad vertex";
		else if (edge < 0 || edge > INT_MAX)
			throw "bad length";
		else if (vertex1 == vertex2 && N == 1)
			throw "bad number of vertices";
		else
		{
			Matrix[vertex1 - 1][vertex2 - 1] = edge;
			Matrix[vertex2 - 1][vertex1 - 1] = edge;
		}
	}
}

unsigned int Dijkstra::Print_Min_Path(std::ostream &file_out) const
{
	int count = 0;
	for (int i = 0; i < N; i++)
	{
		if (min_weight[i] == INT_MAX + 1)
			file_out << "INT_MAX+ ";
		else if (min_weight[i] == INFINITY)
			file_out << "oo ";
		else 
			file_out << min_weight[i] << ' ';

		if (Matrix[Finish - 1][i] != 0) count++;
	}
	return count;
}

void Dijkstra::Find_Path(int count, std::ostream &file_out) const
{
	int i;
	file_out << std::endl;
	if (min_weight[Finish - 1] == INFINITY)
		file_out << "no path";
	else
		if (min_weight[Finish - 1] == INT_MAX + 1 && count == 2)
			file_out << "overflow";
		else
		{
			i = Finish - 1;
			while (path[i] != i)
			{
				file_out << i + 1 << ' ';
				i = path[i];
			}
			file_out << i + 1 << ' ';
		}
}

void Dijkstra::Minimal_Distance(std::ostream &file_out)
{
	color = new bool[N];
	path = new unsigned int[N];
	min_weight = new unsigned int[N];


	for (int i = 0; i < N; i++)
	{
		color[i] = WHITE;
		path[i] = -1;
		min_weight[i] = INFINITY;

	}
	unsigned int min, min_index;
	min_weight[Start - 1] = 0;
	path[Start - 1] = Start - 1;
	do
	{
		min = INFINITY;
		min_index = INFINITY;
		for (int i = 0; i < N; i++)
		{
			if ((color[i] == WHITE) && (min_weight[i] < min))
			{
				min = min_weight[i];
				min_index = i;
			}
		}
		if (min_index < INFINITY)
		{
			for (int i = 0; i < N; i++)
			{
				if (Matrix[min_index][i] > 0)
				{
					unsigned int tmp = (min + Matrix[min_index][i] > INT_MAX) ?
						INT_MAX + 1 : min + Matrix[min_index][i];
					if (tmp < min_weight[i])
					{
						min_weight[i] = tmp;
						path[i] = min_index;
					}
				}
			}
			color[min_index] = RED;
		}
	} while (min_index != INFINITY);

	///����� ���������� ���������� �� ������
	int count = Print_Min_Path(file_out);
	///����� ���� �� Finish �� Start
	Find_Path(count, file_out);
}