#include <iostream>
#include <fstream>
#include "Class_1.h"

using namespace std;
int main()
{
	ifstream file_in("in.txt", ios_base::in);
	ofstream file_out("out.txt", ios_base::out);
	if (file_in.is_open() == false)
		return 1;
	try
	{
		Dijkstra Graph(file_in);
		Graph.Create_Matrix(file_in);
		Graph.Minimal_Distance(cout);
	}
	catch (const char *err)
	{
		file_out << err << endl;
		cin.get();
		return 0;
	}
	cout << "\nAll completed! =)";
	cin.get();
	return 0;
}