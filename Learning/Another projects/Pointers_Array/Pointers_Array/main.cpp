#include <cstdlib>
#include <iostream>
using namespace std;

void swap(int *arr, int i, int j)
{
	int tmp = arr[i];
	arr[i] = arr[j];
	arr[j] = tmp;
}

void qsort(int *arr, int numb_left, int numb_right)
{
	int i = numb_left;
	int j = numb_right;
	int mid = arr[(i + j) / 2];
	while (i<j)
	{
		while (arr[i] < mid) i++;
		while (arr[j] > mid) j--;
		if (i <= j)
		{
			swap(arr, i, j);
			i++;
			j--;
		}
	}
	if (numb_left < j)  qsort(arr, numb_left, j);
	if (numb_right > i) qsort(arr, i, numb_right);
}

int main()
{
	int *a = new int;
	int *b = new int(5);
	*a = 10;
	*b = *a + *b;
	cout << *b << endl;
	delete a;
	delete b;
	// ������������ ������ //
	int num;
	cout << "Input array size: ";
	cin >> num;
	int *arr = new int[num];
	for (int i = 0; i < num; i++)
		cin >> arr[i];

	qsort(arr, 0, num - 1);
	cout << endl;
	for (int i = 0; i < num; i++)
		cout << arr[i] << " ";
	
	delete[] arr;
	system("pause");
	return 0;
}