#include "Stack.h"
#include <iostream>

template <typename T>
Stack<T>::node::node(T value) : data(value)
{}

template <typename T>
Stack<T>::node::~node()
{
	std::cout << "List with value " << data << " was deleted!" << std::endl;
}

template <typename T>
Stack<T>::Stack() : count(0)
{}

template <typename T>
void Stack<T>::push(T value)
{
	node *temp = new node(value);
	count++;

	if (!Head)
		Head = temp;
	else
	{
		temp->next = Head;
		Head = temp;
	}
	std::cout << "Value " << value << " was added!" << std::endl;
}

template <typename T>
void Stack<T>::show() const
{
	node *temp = Head;
	while (temp)
	{
		std::cout << temp->data << " ";
		temp = temp->next;
	}
	std::cout << std::endl;
}

template <typename T>
T Stack<T>::pop()
{
	if (!Head)
	{
		std::cout << "Stack is empty!" << std::endl;
		return -1;
	}
	else
	{
		node *temp = Head;
		int value = temp->data;
		Head = Head->next;
		delete temp;
		count--;
		return value;
	}
}

template <typename T>
void Stack<T>::get_count() const
{
	std::cout << "There are " << count << " elements in the Stack" << std::endl;
}

template <typename T>
Stack<T>::~Stack()
{
	while (Head)
	{
		node *temp = Head;
		Head = Head->next;
		delete temp;
	}
}





