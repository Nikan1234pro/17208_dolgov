#pragma once
#include <iostream>

template <typename T>
class Stack
{
private:
	class node
	{
		friend class Stack<T>;
	private:
		T data;
		node *next;
		node(T value);
		~node();	
	};
	node *Head;
	int count;
public:
	Stack();
	void push(T);
	void show() const;
	void get_count() const;
	T pop();
	~Stack();
};




