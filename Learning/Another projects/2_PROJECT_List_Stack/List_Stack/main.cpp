#include "Stack.cpp"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUS");
	Stack<int> *My_stack = new Stack<int>;

	My_stack->get_count();
	My_stack->push(10);
	My_stack->push(12);
	My_stack->push(98);
	My_stack->show();
	My_stack->pop();
	My_stack->show();
	My_stack->get_count();
	delete My_stack;
	system("pause");
	return 0;
}