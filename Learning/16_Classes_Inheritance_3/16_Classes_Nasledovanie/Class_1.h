#pragma once
#include <iostream>
#include <string>
class employee
{
protected:
	std::string name;
	int age;
public:
	void getdata()
	{
		std::cout << "Input name: ";
		std::cin >> name;

		std::cout << "Input age: ";
		std::cin >> age;
	}

	void showdata()
	{
		std::cout << "Name: " << name << std::endl;
		std::cout << "Age: " << age << std::endl;
	}
};

class manager : private employee
{
private:
	int number;
public:
	void getdata()
	{
		employee::getdata();
		std::cout << "Input number: ";
		std::cin >> number;
	}
	void showdata()
	{
		employee::showdata();
		std::cout << "Number: " << number << std::endl;
	}
};
