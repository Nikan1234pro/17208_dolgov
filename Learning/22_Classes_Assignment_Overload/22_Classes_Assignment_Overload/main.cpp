#include "class.h"
#include "class2.h"
int n = 10;

using namespace std;
int main()
{
	setlocale(LC_ALL, "RUSSIAN");
	///////////////////////////////////////
	Object a1 = 56;
	Object a2;

	a2 = a1;
	cout << "\na2 = ";
	a2.get();

	Object a3 = a2;
	cout << "\na3 = ";
	a3.get();
	////////////////////////////////////////
	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";

	alpha b1(37);
	alpha b2;

	b2 = b1; 
	cout << "\nb2 = "; 
	b2.display(); 

	alpha b3 = b1;
	cout << "\nb3 = "; 
	b3.display(); 

	cout << endl;
	system("pause");
	return 0;
}