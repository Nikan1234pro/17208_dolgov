#pragma once
#include <iostream>
#include <string>
class people
{
private: //������ ���� ����������
	int age;
	std::string name;
public:
	void getdata()
	{
		std::cout << "Input age: ";
		std::cin >> age;
		std::cout << "Input name: ";
		std::cin >> name;
	}
	void putdata()
	{
		std::cout << std::endl;
		std::cout << age << std::endl;
		std::cout << name << std::endl;
	}
};

class employee : public people
{
private:
	int salary;
	std::string work;
public:
	void getdata()
	{
		//std::cin >> age; - ��� ������
		people::getdata(); // - � ��� �����

		std::cout << "Input salary: ";
		std::cin >> salary;
		std::cout << "Input work: ";
		std::cin >> work;
	}

	void putdata()
	{
		people::putdata();
		std::cout << salary << std::endl;
		std::cout << work << std::endl;
	}
};

/*
������ ������������ ������ ����� ������ � ������ �������� ������, 
���� ��� ����� ������������ ������� public ��� protected. 
� ������, ����������� ��� private, ������� ���.
*/