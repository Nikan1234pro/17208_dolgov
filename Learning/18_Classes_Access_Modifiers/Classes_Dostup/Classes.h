#pragma once
#pragma once
class A
{
protected:
	static const int prot_val = 10;
private:
	static const int priv_val = 20;
public:
	static const int publ_val = 30;
};


class B : public A
{
public:
	int Test()
	{
		int a;
		a = prot_val;
		//a = priv_val; - ������
		a = publ_val;
	}
};

class C : private A
{
public:
	int Test()
	{
		int a;
		a = prot_val;
		//a = priv_val; - ������
		a = publ_val;
	}
};