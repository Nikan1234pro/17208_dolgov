#pragma once
#include <iostream>
#include <cstring>
class strCount
{
private:
	int count;
	char *str;
	friend class String;

	strCount(const char *s)
	{
		int len = strlen(s);
		str = new char[len + 1];
		strcpy(str, s);
		count = 1;
	}

	~strCount()
	{
		delete[] str;
	}
};

class String
{
private:
	strCount * ptr;
public:
	String()
	{
		ptr = new strCount("NULL");
	}

	String(const char *s)
	{
		ptr = new strCount(s);
	}

	String(String &s)
	{
		ptr = s.ptr;
		ptr->count++;
	}

	String& operator=(String &s)
	{
		if (this == &s)
			return *this; //���� ������ � ����� ���� ������, �� �����

		if (ptr->count == 1)
			delete ptr;
		else
			ptr->count--;
		ptr = s.ptr;
		ptr->count++;
		return *this;
	}
	~String()
	{
		if (ptr->count == 1)
			delete ptr;
		else
			ptr->count--;
	}
	void display()
	{
		std::cout << ptr->str << std::endl;
		std::cout << "Addr: " << ptr << std::endl
				  << std::endl;
	}
};