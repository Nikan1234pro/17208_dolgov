#define _CRT_SECURE_NO_WARNINGS
#include "beginning.h"
#include "second_lesson.h"
#include "third_lesson.h"
using namespace std;
int main()
{
	setlocale(LC_ALL, "RUS");
	Obj w1, w2, w3;
	w1.get_addr();
	w2.get_addr();
	w3.get_addr();

	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;

	alpha a1 = 12;
	alpha a2 = 34;
	alpha a3 = 76;
	a1 = a2 = a3;   //������ operator= ������������ alpha& ����� ��� �������� =
					//��� �������� = ������ ��� void
					//�� ������ ������ ����� alpha&

	a2.display();
	a3.display();

	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;

	String s1 = "������, ���� ����� ������";
	s1 = s1; //���� �� ������� �������� ������� � operator, �� ��� ��� ���������

	String s2;
	String s3 = "Hi";
	s1.display();
	s2.display();
	s3.display();

	s2 = s1;
	s2.display();

	s1 = s2 = s3;
	s1.display();

	cin.get();
	return 0;
}