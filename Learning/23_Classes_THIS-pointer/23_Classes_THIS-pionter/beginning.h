#pragma once
#include <iostream>
class Obj
{
private:
	char arr[10];
public:
	void get_addr()
	{
		std::cout << "My address is: " << this << std::endl;
	}
};

class Obj2
{
private:
	int alph;
public:
	void test()
	{
		this->alph = 99;
		std::cout << this->alph << std::endl;
				 
	}
};