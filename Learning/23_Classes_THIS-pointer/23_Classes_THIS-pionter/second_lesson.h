#pragma once
#include <iostream>
class alpha
{
private:
	int data;
public:
	alpha()
	{}

	alpha(int n) : data(n)
	{}

	void display()
	{
		std::cout << data << std::endl;
	}

	alpha& operator=(alpha &a)
	{
		std::cout << "Запущен оператор присваивания!" << std::endl;
		data = a.data;
		return *this;
	}

};