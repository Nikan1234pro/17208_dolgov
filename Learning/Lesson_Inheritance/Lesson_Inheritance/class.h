#pragma once
#include <iostream>

class Base
{
public:
	void f(double d)
	{
		std::cout << "double" << std::endl;
	}

	void f(int i)
	{
		std::cout << "int" << std::endl;
	}
};

class Derv : public Base
{
public:
	using Base::f;  //���� �� ��������, �� f(char) ��������� ���������
	void f(char c)
	{
		std::cout << "char" << std::endl;
	}
};