#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;
int main()
{
	setlocale(LC_ALL, "");

	vector<string> students;
	string buffer = "";
	do
	{
		getline(cin, buffer);
		if (buffer.size())
			students.push_back(buffer);
	} while (buffer != "");

	int size = students.size();
	
	for (int i = 0; i < size; i++)
		cout << students[i] << endl; 

	students.clear();
	system("pause");
	return 0;
}