#pragma once
/*
�������� ����������� ������,
������������� � ������� ��������� �� 
��������� ������ ������
*/
#include <iostream>
struct elem
{
	int key;
	elem *next;
};

class List
{
private:
	elem *first;
public:
	List() : first(NULL)
	{}

	~List()
	{
		while (first)
			pop();
		std::cout << "All Cleared!" << std::endl;
	}

	void push(int d)
	{
		elem *temp = new elem;
		temp->key = d;
		temp->next = first;
		first = temp;
	}

	void pop()
	{
		if (first)
		{
			elem *temp = first;
			first = first->next;
			delete temp;
		}
		else
			std::cout << "Nothing to delete!" << std::endl;
	}

	void showlist() const
	{
		elem *cur = first;
		while (cur)
		{
			std::cout << cur->key << std::endl;
			cur = cur->next;
		}		
	}
};