#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

const int num = 5;
class Students
{
	public:
		Students(string Name); //Конструктор
		~Students(); //Деструктор
		void save();
		void set_name(string cur_name);
		string get_name();
		void set_scores(int cur_scores[num]);
		void set_result(double ball);
		double get_result();
	private:
		string name;
		int scores[num];
		double result;
};