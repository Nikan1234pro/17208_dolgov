#pragma once
#include <iostream>
template <typename T>
class Stack
{
private:
	T* st;
	int top;
	int size;
public:
	Stack(int = 10);
	void push(T);
	T pop();
};

template <typename T>
Stack<T>::Stack(int Max_size) :
	size(Max_size), top(0)
{
	st = new int[size];
}

template <typename T>
void Stack<T>::push(T value)
{
	if (top == size)
		std::cout << "Stack is full" << std::endl;
	else
		st[top++] = value;
}

template <typename T>
T Stack<T>::pop()
{
	if (!top)
	{
		std::cout << "Stack is empty!" << std::endl;
		return -1;
	}	
	else
		return st[--top];
}