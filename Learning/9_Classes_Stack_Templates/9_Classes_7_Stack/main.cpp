#include <iostream>
#include "Stack.h"
#include <conio.h>
using namespace std;
int main()
{
	Stack<int> my_stack(2);
	my_stack.push(4);
	my_stack.push(3);
	my_stack.push(7);

	cout << my_stack.pop() << endl;
	cout << my_stack.pop() << endl;
	cout << my_stack.pop() << endl;

	_getch();
	return 0;
}