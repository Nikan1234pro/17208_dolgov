#include <iostream>
#include <fstream>
using namespace std;
const int MAX = 100;
void put_data(int *buf)
{
	ofstream file("test.txt", ios::binary);
	file.write(reinterpret_cast<char*>(buf), MAX * sizeof(int));
	for (int i = 0; i < MAX; i++)
		buf[i] = 0;
	file.close();
}

void get_data(int *buf)
{
	ifstream file("test.txt", ios::binary);
	file.read(reinterpret_cast<char*>(buf), MAX * sizeof(int));
	for (int i = 0; i < MAX; i++)
		cout << buf[i] << ' ';
	cout << endl;
	file.close();
}

int main()
{
	int *buf = new int[MAX];
	for (int i = 0; i < MAX; i++)
		buf[i] = i * 10;
	//put_data(buf);
	get_data(buf);
	system("pause");
	return 0;
}
/*
� ��������� ������� ��������, ��� � �������� ���� ������ ����� ����� ������������ � ���� 
� �������� �� ����. ��� ���� ������������ ��� ������� � write() (����� ������ ofstream), 
� ����� read() (����� ifstream). ��� ������� ������ � ������ � �������� ������ (��� char). 
�� ��� �����, ��� ������������ ������, ��� ��� ����� ������������, � ��� ������ ��������� 
����� �� ������ � ���� � �������. ����������� ���� ������� �������� ����� ������ � ��� �����. 
����� ������ ���� �������� � �������������� reinterpret_cast ������������ ���� char*. 
������ ���������� �������� ����� � ������ (� �� ����� ��������� ������ � ������).

��������� ���������� �������� reinterpret_ cast ��� ����, ����� ����� ������ ���� int �������� ��� 
������� read() � write() ��� ����� ���� char.
	file.read(reinterpret_cast<char*>(buff), MAX*sizeof(int));
reinterpret_cast �������� ��� ������ � ������������ ������� ������, ���������� �� ����������� � ���, 
����� ��� ����� ��� ���. ������� ������ ���������������� ������������� ����� ��������� �������� ������� 
�� ������� ������������. ����� ������������ reinterpret_cast ��� ����������� ���������� � ������ 
���� int � �������. ��� ������������ �������, �� ����� ��� ����������.
*/