#include <iostream>
#include <conio.h>
using namespace std;

template <typename T> // !!!
T max_arr(T*, int);

int main()
{
	int iArr[5] = { 5, 7, 2, 8, 4 };
	char cArr[6] = "abcdk";
	cout << max_arr(iArr, 5) << endl;
	cout << max_arr(cArr, 5) << endl;
	_getch();
	return 0;
}

template <typename T>
T max_arr(T *arr, int n)
{
	T max = arr[0];
	for (int i = 0; i < n; i++)
		if (arr[i] > max)
			max = arr[i];
	return max;
}

