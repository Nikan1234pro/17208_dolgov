#pragma once
#include <iostream>
class Line
{
private:
	int *arr;
	int len;
public:
	Line() : len(0)
	{}

	Line(int l, int k) : len(l)
	{
		arr = new int[len];
		for (int i = 0; i < len; i++)
			arr[i] = k * 10 + i;
	}

	int& operator[](int i) const
	{
		return arr[i];
	}
};

class Matrix
{
private:
	int width;
	int height;
	Line *arr;
public:
	Matrix() : width(0), height(0), arr(NULL)
	{}

	Matrix(int h, int w) : width(w), height(h)
	{
		arr = new Line[height];

		for (int i = 0; i < height; i++)
			arr[i] = Line(width, i);
	}

	Line& operator[](int i) const
	{
		return arr[i];
	}

};