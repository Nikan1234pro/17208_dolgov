#include "Class.h"
#include <iomanip>
int main()
{
	Matrix M(10, 10);
	M[4][7] = 3228;
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
			std::cout << std::setw(5)
			<< std::setiosflags(std::ios::left) 
			<< M[i][j];
		std::cout << std::endl;
	}
	
	system("pause");
	return 0;
}