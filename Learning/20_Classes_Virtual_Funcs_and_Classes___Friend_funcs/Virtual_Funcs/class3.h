#pragma once
#include <iostream>
class Main
{
public:
	//~Main()
	virtual ~Main()
	{
		std::cout << "Main is deleted!" << std::endl;
	}
};

class Son : public Main
{
public:
	~Son()
	{
		std::cout << "Son is deleted!" << std::endl;
	}
};