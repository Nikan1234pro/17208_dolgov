#include "class_1.h"
#include "class_2.h"
#include "class3.h"
#include "class5.h"
#include "class6.h"
void test1();
void test2();
void test3();
void test4();
void test5();

int main()
{
	test1();
	test2();
	test3();
	test4();
	test5();

	system("pause");
	return 0;
}


void test1()
{
	Derv1 dv1; // ������ ������������ ������ 1
	Derv2 dv2; // ������ ������������ ������ 2
	Base* ptr; // ��������� �� ������� �����

	ptr = &dv1; // ����� dv1 ������� � ���������
	ptr->show(); // ��������� show()
	ptr = &dv2; // ����� dv2 ������� � ���������
	ptr->show(); // ��������� show()


	Base *arr[2];
	arr[0] = &dv1;
	arr[1] = &dv2;
	for (int i = 0; i < 2; i++)
		arr[i]->show();
	cout << endl;
	cout << endl;
}

void test2()
{
	person *arr[100];
	int n = 0;
	char ch;
	do
	{
		char id;
		cout << "Student(s) / Professor(p): ";
		cin >> id;
		if (id == 's')
			arr[n] = new student;
		else if (id == 'p')
			arr[n] = new professor;
		else
			//continue;
			break;
		arr[n++]->getData();

		cout << "Continue? (y / n): ";
		cin >> ch;

	} while (ch != 'n');

	for (int i = 0; i < n; i++)
	{
		if (arr[i]->isOutstanding())
		{
			arr[i]->putName();
			cout << " is respectful!" << endl;
		}
	}
}

void test3()
{
	cout << endl;
	cout << endl;
	Main *ptr = new Son;
	delete ptr; // ���� ���������� �� �����������, �� ��������� ���������� ��� Main
}

void test4()
{
	alfa a;
	beta b;
	cout << frFunc(a, b) << endl;
}

void test5()
{
	Distance d1 = 2.67;
	Distance d2(14.12);
	Distance d3 = d1 + 10.0; //������ ����

	Distance d4 = 10.0 + d2; //��� ����, �.�. �������� + friend
}
