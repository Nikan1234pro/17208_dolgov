#pragma once
#include <string>;
#include <iostream>

using namespace std;
class person
{
private:
	string name;

public:
	void getName()
	{
		cout << "Input name: ";
		cin >> name;
	}

	void putName()
	{
		cout << "Name: " << name << endl;
	}

	virtual void getData() = 0;
	virtual bool isOutstanding() = 0;

};

class student : public person
{
private:
	double ball;

public:
	void getData()
	{
		person::getName();
		cout << "Input average ball: ";
		cin >> ball;
	}

	bool isOutstanding()
	{
		return (ball > 4.0) ? true : false;
	}
};

class professor : public person 
{
private:
	int numPubs; 
public:
	void getData() 
	{ 
		person::getName();
		cout << "Count of publications: "; 
		cin >> numPubs;
	}

	bool isOutstanding()
	{
		return (numPubs > 100) ? true : false;
	}
};