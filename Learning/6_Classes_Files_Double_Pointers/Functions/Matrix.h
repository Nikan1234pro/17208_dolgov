#ifndef MATRIX_H //������ �� ���������� ���������
#define MATRIX_H

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

class Matrix
{
	public:
		Matrix(int n, int m);
		~Matrix();
		void add_data(std::ifstream &file);
		void print();

	private:
		int **M;
		int width;
		int height;
};

#endif // !MATRIX_H