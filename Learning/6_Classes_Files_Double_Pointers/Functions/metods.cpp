#include "Matrix.h"

Matrix::Matrix(int n, int m) : width(n), height(m) //������ ������������� ����� ������ � ���, � � { } ��� ������ ���������
{
	M = new int*[width];
	for (int i = 0; i < width; i++)
		M[i] = new int[height];

	for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++)
			M[i][j] = 0;
}

Matrix::~Matrix()
{
	for (int i = 0; i < width; i++)
		delete M[i];
	delete M;
	std::cout << "All cleared" << std::endl;
}

void Matrix::add_data(std::ifstream &file)
{
	int number = 0;
	int vertex1 = 0, vertex2 = 0, edge = 0;
	file >> number;
	for (int i = 0; i < number; i++)
	{
		file >> vertex1 >> vertex2 >> edge;
		M[vertex1 - 1][vertex2 - 1] = edge;
	}
}

void Matrix::print()
{
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
			std::cout << std::setw(2) << M[i][j];
		std::cout << std::endl;
	}
}
