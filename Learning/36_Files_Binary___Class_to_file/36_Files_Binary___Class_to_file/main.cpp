#include <iostream>
#include <fstream>
#include "class.h"
using namespace std;
void to_file()
{
	person *pers = new person;
	//cout << sizeof(*pers);
	pers->getdata();
	ofstream file("test.txt", ios::binary);
	file.write(reinterpret_cast<char*>(pers), sizeof(*pers));
	file.close();
	cout << "All completed! \n";
}

void from_file()
{
	ifstream file("test.txt", ios::binary);
	person pers;
	file.read(reinterpret_cast<char*>(&pers), sizeof(pers));
	pers.showdata();
	file.close();
	cout << "End! \n";
}

/*
������� seekg() � tellg() ��������� ������������� � ��������� ������� ��������� ������, � 
������� seekp() � tellp() � ��������� �� �� �������� ��� ��������� ������.
*/

int main()
{
	to_file();
	from_file();
	system("pause");
	return 0;
}