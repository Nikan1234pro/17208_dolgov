#pragma once
#include <iostream>
#include <string>
class person
{
protected:
	char name[10];
	int age;
public:
	void getdata()
	{
		std::cout << "Input name: ";
		std::cin >> name;
		std::cout << "Input age: ";
		std::cin >> age;
	}

	void showdata()
	{
		std::cout << "Name: " << name << std::endl;
		std::cout << "Age: " << age << std::endl;	
	}
};

/*
�� ����� ��������, ����� ������������ ������ � �������, � ��� � ���� ������ � ������� �� ������������.

���� �� ������ � ���� � ������� ������� ����������� �������, ���������� ��������� ������� ������������. 
� ���� �������� ���� ���������� �����, ������� �������� ����� ������� �� ������� ������ � ������. 
��� �������� ���������������� ����� ������� ��� ������������� ����������� �������. 
����� �� ����������� ������ � ����, ��� ����� ������������ ������ � ������� �������. ���� �������� 
������ ������, ������������� ����� ����������. ���� � ������ ������� ����������� �������, �� ��� ������ 
������� � ���� �� �������, �� ������� �������� ��������� ������� ���������. ������ ������: �����, 
�������������� ��� ������ �������, ������ ���� ��������� ������, ����������������� ��� ��� ������.
*/
