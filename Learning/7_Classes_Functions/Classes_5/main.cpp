///////////////////////////
// �������� ������ �     //
// �������� ���������    //
// �������               //
///////////////////////////

#define parameter 1 //���������� ���������� �� ��������� ��������
#define variant 0 //����������, ���������� �� ������� add_dist

#include <iostream>
using namespace std;
class Distance
{
	public:
		Distance() : feet(0), inches(0)
		{ }
		Distance(int a, float b) : feet(a), inches(b)
		{ }
		void get_dist()
		{
			cout << "\n������� ����� �����: "; cin >> feet;
			cout << "������� ����� ������: "; cin >> inches;
		}
		void showdist()
		{
			cout << feet << "\'-" << inches << '\"';
		}
		#if variant 
		void add_dist(Distance, Distance);
		#else 
		Distance add_dist(const Distance&) const;
		#endif
	private:
		int feet;
		float inches;

};

#if variant
void Distance::add_dist(Distance dd1, Distance dd2) //�����-�� ������ ������, �� ��� �� �����, ����� �������� ������ � ��������
{													//��������� �������
	inches = dd1.inches + dd2.inches; 
	feet = 0; 
	if (inches >= 12.0) 
	{ 
		inches -= 12.0; 
		feet++; 
	}
	feet += dd1.feet + dd2.feet; 
}
#else
Distance Distance::add_dist(const Distance& dd) const
{
	Distance temp;
	temp.inches = inches + dd.inches;
	temp.feet = 0;
	if (temp.inches >= 12.0)
	{
		temp.inches -= 12.0;
		temp.feet = 1;
	}
	temp.feet += feet + dd.feet;
	return temp;

}
#endif


int main()
{
	setlocale(LC_ALL, "RUS");
#if parameter
		Distance dist1, dist3;
		Distance dist2(11, 6.25);
		dist1.get_dist();
		#if variant
			dist3.add_dist(dist1, dist2);
		#else
			dist3 = dist1.add_dist(dist2);
		#endif
#else

		Distance dist1(11, 6.25); 
		Distance dist2(dist1);  //������������ ����������� ����������� �� ���������
		Distance dist3 = dist1; //����������� ������ ������� � ������
#endif
	cout << "\ndist1 = "; dist1.showdist();
	cout << "\ndist2 = "; dist2.showdist();
	cout << "\ndist3 = "; dist3.showdist();
	cout << endl;
	system("pause");
	return 0;
}

/*
1) ��������� ������� �������� �� ������������� ��� ����� �������������� ��������. 
����� ������ ������� add_dist() ����� ��� ������ Distance � ����� ������ ::. 
���� ������ �������� ������ �������� ����������� ����������. ����� ����� ������ 
������������� ����������� ������� � ������, � ������� �������- �� ��� �������. 
� ������ ������ ������ Distance::add_dist() ��������, ��� ����- ��� add_dist() 
�������� ������� ������ Distance.



2) Distance dist2(dist1);
�������� ����������� ������������ �� ��������� �������� � ����������� �������� 
����� ������� dist1 � ��������������� ���� ������� dist2. ��� ��� �� �����������, 
�� ���������� �������� ��� ���� �������� dist1 � dist3 ����������� ��� ������ ���������
Distance dist3 = dist1;
*/