#pragma once
#include <iostream>
#include <typeinfo> // ��� typeid()
#include <cstring>
class objA
{
private:
	int data;
	virtual void vertFunc()
	{}
};

class objB : public objA
{};

class objC : public objA
{};

void displayName(objA *ptr)
{
	std::cout << "������ ����:"
		<< typeid(*ptr).name() << std::endl;
}