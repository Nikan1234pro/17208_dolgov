#pragma once
#include <typeinfo> // ��� dynamic_cast
#include <iostream>
class Base
{
	virtual void vertFunc() // ��� dynamic cast
	{}

public:
	void test()
	{
		if (typeid(Base) == typeid(*this))
			std::cout << "YES" << std::endl;
		else
			std::cout << "NO" << std::endl;
	}
};

class Derv1 : public Base
{};

class Derv2 : public Base
{};

bool isDerv1(Base* pUnknown) 
{
	Derv1  *pDerv1;
	if (pDerv1 = dynamic_cast<Derv1*>(pUnknown))
		return true;
	else
		return false;
}


/*
� ��� ���� ������� ����� Base � ��� ����������� � Derv1 � Derv2. ���� ����� ������� isDerv1(), 
������������ �������� �������, ���� ���������, ���������� � �������� ���������, ��������� �� ������ ������ Derv1. 
��� �������� ����������� ������ Base, ������� ������� ����� ���� ���� ������ Derv1, ���� Derv2.
�������� dynamic_cast �������� �������� ��������� ������������ ���� pUnknown � ���� Derv1. 
���� ��������� ���������, ������, pUnknown �������� �� ������ Derv1. � ��������� ������ �� �������� �� ���-�� ������.
*/