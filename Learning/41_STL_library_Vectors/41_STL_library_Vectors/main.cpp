#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<int> v;
	v.push_back(10);
	v.push_back(34);
	v.push_back(47);
	v.push_back(11);

	cout << "Max = " << v.max_size() << endl;

	v[0] = 1;
	for (int i = 0; i < v.size(); i++)
		cout << v[i] << ' ';
	cout << endl;

	double a[] = { 1.1, 2.345, 67.6, 34.23 };
	vector<double> v1(a, a + 4); //������������� �������
	vector<double> v2(4); //������ �� 4 ��-��
	v1.swap(v2); // �������� ���������� v1 � v2
	
	v2.insert(v2.begin() + 1, 228.2);
	v2.erase(v2.begin());

	while (!v2.empty())
	{
		cout << v2.back() << ' ';
		v2.pop_back();
	}
	cout << endl;

	

	system("pause");
	return 0;
}
/*
����� insert() (�� ������� ����, ������ ������) 
����� ��� ���������: ������� ������������ ������ �������� � ���������� � �������� ��������.
*/