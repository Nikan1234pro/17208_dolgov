#include <iostream> // ��������� �������������
#include <iomanip> 
#include <process.h> // ��� exit()
using namespace std; // ���������
// ��������, ��� ��� ������������ ���� ����� � ��������� ����� ���������� � ������������
// ���� � ������ std
void id(int number)
{
	cout << "\n\n\n";
	cout << "/////////" << endl;
	cout << "STEP " << number << endl;
	cout << endl;
}

int main()
{
	/// 1 ///
	id(1);

	cout << "Hello\
 world!" << endl; //������� ������ (�� ����� ��� �� ������)

	// << - � �� ��� ��������� �����, �� ��� ������ ������ ���� �� ���� ���������� �������
	// cout - ����������� ����� ������ (�����)

	/// 2 ///
	id(2);
	cout << '\'' << endl; // ������� �������

	/// 3 ///
	id(3);
	double k = 1.2E-3;
	cout << k << endl;

	int a = 5;
	int b = 7;
	bool test = (a < b);	//��� bool
	cout << test << endl;

	/// 4 ///
	id(4);
	cout << setw(6) << "Hello" << endl;

	cout << setw(9) << "Test_" 
		 << setiosflags(ios::left)  // �������� � ������ ����
		 << setw(9) << "Test" << endl;
	// #include <iomanip>  - ��� ������ setw
	
	/// 5 ///
	id(5);
	char var1 = 0;
	int var2 = 128;
	var1 = static_cast<char>(var2); //���������� �����
	//����� ���� (char)var2 - �� �� �������������
	cout << var1 << endl;

	/// 6 ///
	id(6);
	int v1, v2;
	char ch;
	do
	{	
		cin >> v1;
		cin >> v2;
		cout << v1 * v2 << endl;
		cout << "Continue? (y / n)" << endl;
		cin >> ch;
	} while (ch != 'n');

	/// 7 ///
	int ou;
	cin >> ou;
	if (!ou)
		exit(0); //����� �� ���������

	system("pause");
	return 0;
}