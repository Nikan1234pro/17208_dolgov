//	t24 -> t12	//
#define var 0
#if var
#include "Time.h"
#else
#include "Time_2.h"
#endif
int main()
{
	time24 t24(19, 34, 27);
	t24.display();

	time12 t12 = t24;
	t12.display();
	system("pause");
	return 0;
}