#pragma once
#pragma once
#include <iostream>
#include <string>
class time24
{
public:
	time24() : hours(0), minutes(0), seconds(0)
	{}

	time24(int h, int m, int s) : hours(h), minutes(m), seconds(s)
	{}

	void display() const
	{
		if (hours < 10)
			std::cout << '0';
		std::cout << hours << ':';

		if (minutes < 10)
			std::cout << '0';
		std::cout << minutes << ':';
		if (seconds < 10)
			std::cout << '0';
		std::cout << seconds << std::endl;
	}
	int get_hrs()
	{return hours;}
	int get_mins()
	{return minutes;}
	int get_secs()
	{return seconds;}
private:
	int hours;
	int minutes;
	int seconds;
};

class time12
{
public:
	time12() : pm(true), hours(0), minutes(0)
	{}

	time12(bool ap, int h, int m) : pm(ap), hours(h), minutes(m)
	{}

	time12(time24&);

	void display() const
	{
		std::string ap = (pm) ? "pm" : "am";
		std::cout << hours << ':';
		std::cout << minutes;
		std::cout << ap << std::endl;
		
	}

private:
	bool pm;
	int hours;
	int minutes;
};



 //���� �������� ���������� � ������ ����������
time12::time12(time24 &T)
{
	int hrs24 = T.get_hrs(); 
							  
	pm = T.get_hrs() < 12 ? false : true;
	minutes = (T.get_secs() < 30) ? 
		T.get_mins() : T.get_mins() + 1;
	if (minutes == 60) 
	{
		minutes = 0;
		++hrs24;
		if (hrs24 == 12 || hrs24 == 24) 
			pm = (pm == true) ? false : true; 
	}
	hours = (hrs24 < 13) ? hrs24 : hrs24 - 12; 
	if (hours == 0) 
	{
		hours = 12; pm = false;
	}
}
