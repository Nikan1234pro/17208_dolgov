#pragma once
#include <iostream>
#include <string>
#define var 1
class time12
{
public:
	time12() : pm(true), hours(0), minutes(0)
	{}

	time12(bool ap, int h, int m) : pm(ap), hours(h), minutes(m)
	{}

	void display()
	{
		std::string ap = (pm) ? "pm" : "am";
		std::cout << hours << ':';
		std::cout << minutes;
		std::cout << ap << std::endl;
	}

private:
	bool pm;
	int hours;
	int minutes;
};

class time24
{
public:
	time24() : hours(0), minutes(0), seconds(0)
	{}

	time24(int h, int m, int s) : hours(h), minutes(m), seconds(s)
	{}
	operator time12() const;

	void display()
	{
		if (hours < 10)
			std::cout << '0';
		std::cout << hours << ':';
		
		if (minutes < 10)
			std::cout << '0';
		std::cout << minutes << ':';
		if (seconds < 10)
			std::cout << '0';
		std::cout << seconds << std::endl;
	}
private:
	int hours;
	int minutes;
	int seconds;
};

// ���� �������� ���������� � �������� ������
time24::operator time12() const
{
	int hrs24 = hours;
	bool pm = hours < 12 ? false : true;
	int roundMins = seconds < 30 ? minutes : minutes + 1;
	if (roundMins == 60) 
	{
		roundMins = 0;
		++hrs24;
		if (hrs24 == 12 || hrs24 == 24) 
			pm = (pm == true) ? false : true; // ������������� am/pm
	}
	int hrs12 = (hrs24 < 13) ? hrs24 : hrs24 - 12;
	if (hrs12 == 0) // 00 ��� 12 a.m.
	{
		hrs12 = 12;
		pm = false;
	}
	return time12(pm, hrs12, roundMins);
}
