using namespace std;
#include <iostream>

class Distance // ����� ���������� ��� �����
{
private:
	float MTF; // ����������� �������� ������ � ����
	int feet;
	float inches;
public:
	// ����������� ��� ����������
	Distance() : feet(0), inches(0.0), MTF(3.280833F)
	{ }
	// ����������� � ����� ����������,
	// ����������� ����� � ���� � �����
	explicit Distance(float meters) : MTF(3.280833F)
	{
		float fltfeet = MTF * meters; // ��������� � ����
		feet = int(fltfeet); // ����� ����� ������ �����
		inches = 12 * (fltfeet - feet); // ������� � ��� �����
	}
	// ����������� � ����� �����������
	Distance(int ft, float in) : feet(ft), inches(in), MTF(3.280833F)
	{ }

	friend istream& operator>>(istream &s, Distance &d)
	{
		cout << "Input feet: ";
		s >> d.feet;
		cout << "Input inches: ";
		s >>d. inches;
		return s;
	}
	// ��������� ���������� �� ������������
	void getdist()
	{
		cout << "\n������� ����: "; cin >> feet;
		cout << "������� �����: "; cin >> inches;
	}
	// ����� ����������
	void showdist() const
	{
		cout << feet << "\'-" << inches << '\"';
	}
	// �������� �������� ��� ��������� ������ �� �����
	operator float() const
	{
		float fracfeet = inches / 12; // ��������� ����� � ����
		fracfeet += static_cast<float>(feet); // ��������� ����� ����
		return fracfeet / MTF; // ��������� � �����
	}
};