#pragma once
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <conio.h>
using namespace std;

class line
{
private:
	int len;
	string str;
public:
	

	line() : str(""), len(0)
	{}

	line(string S) : str(S), len(S.length())
	{}

	char& operator[](int i)
	{
		return str[i];
	}

	string getline()
	{
		return str;
	}
};

class buffer
{
private:
	int count;
	line* ptr_line;
public:
	buffer() : count(0), ptr_line(NULL)
	{}

	buffer(ifstream &file_in)
	{
		file_in >> count;
		file_in.ignore(10, '\n');
		ptr_line = new line[count];
		
		for (int i = 0; i < count; i++)
		{
			string buf;
			getline(file_in, buf);
			ptr_line[i] = line(buf);
		}
			
	}

	int get_count()
	{
		return count;
	}

	void display()
	{
		for (int i = 0; i < count; i++)
		{
			cout << ptr_line[i].getline() << endl;
		}
	}

	void print_to_file(ofstream &file_out)
	{
		for (int i = 0; i < count; i++)
		{
			file_out << ptr_line[i].getline() << endl;
		}
		file_out << endl;
	}

	line& operator[](int i)
	{
		return ptr_line[i];
	}
};