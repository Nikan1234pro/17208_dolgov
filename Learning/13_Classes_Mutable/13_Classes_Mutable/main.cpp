#include <iostream>
#include <string>
using namespace std;

class Test
{
public:
	Test(int sz, string str) : size(sz), buf(str)
	{}

	void set_Name(string name) const
	{
		buf = name;
	}
	void set_Size(int sz) 
	{
		size = sz;
	}

	void get_name() const
	{
		cout << buf << endl;
	}
private:
	int size;
	mutable string buf;
};

int main()
{
	const Test K(10, "Vasya");
	K.set_Name("Nikita"); //���� �.�. mutable
	//K.set_Size(10); - ������
	K.get_name();
	system("pause");
	return 0;
}