/////////////////////////////
//     ����������          //
//      �������            //
///////////////////////////// 
#include <iostream>
void print_ch()
{
	for (int i = 0; i < 20; i++)
		std::cout << '*';
	std::cout << std::endl;
}

void print_ch(char s)
{
	for (int i = 0; i < 20; i++)
		std::cout << s;
	std::cout << std::endl; 
}

void print_ch(int n, char s)
{
	for (int i = 0; i < n; i++)
		std::cout << s;
	std::cout << std::endl;
}

int main()
{
	print_ch();
	print_ch('+');
	print_ch(10, '-');
	system("pause");
	return 0;
}