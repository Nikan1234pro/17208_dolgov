#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
	setlocale(LC_ALL, "RUS");

	// ����� �������������� IOS //
	bool log = true;
	cout.setf(ios::boolalpha); //������� ����������� 0 � 1 �������������� � �false� � �true�
	cout << log << endl;
	cout.unsetf(ios::boolalpha);


	cout.setf(ios::left); // ������������ ������ �� ������ ����     
			//ios::right
	cout << "���� ����� �������� �� ������ ����" << endl;
	cout.unsetf(ios::left); // ��������� � �������� ��������������

	int a = 0x2F;
	cout.setf(ios::showbase); // ���������� ��������� �.�.
	cout.setf(ios::uppercase); //���������� � ������� ������� ����� X, � � ����� ����������������� ������� ��������� (ABCDEF)
	cout << hex << a << endl;
	cout.unsetf(ios::showbase);

	double b = 12.3892;
	cout.setf(ios::scientific); //���������������� ����� ����� � ��������� �������
	cout << b << endl;
	cout.unsetf(ios::scientific);
	cout.setf(ios::fixed); //������������� ����� ����� � ��������� �������
	cout << b << endl;

	// ������������ //
	cout << setw(4)
	<< setiosflags(ios::right)
	<< dec << a << endl;

	cout << setprecision(2) << b << endl;

	cin.get();
	return 0;
}