#include <iostream>
#include <algorithm>
int arr[] = { 10, 12, 33, 45, 33, 123, 45, 33, 123 };
int pattern[] = { 33, 45, 33 };

bool is_Numb(int number)
{
	return number == 45;
}

void multi10(int n)
{
	std::cout << n * 10 << ' ';
}

int dble(int n)
{
	return n * 2;
}

using namespace std;
int main()
{
	setlocale(LC_ALL, "RUS");
	int *ptr = find(arr, arr + 9, 123);
	cout << "������� 123 ��������� �� ������ " << ptr - arr << endl;

	// // // // // // // // // // // // // // // // // // // // // // // 

	int n = count(arr, arr + 9, 33);
	cout << "������� 33 ����������� " << n << " ���(�)" << endl;

	// // // // // // // // // // // // // // // // // // // // // // // 

	ptr = search(arr, arr + 9, pattern, pattern + 3);
	cout << "������������������ ���������� � ������ " << ptr - arr << endl;

	// // // // // // // // // // // // // // // // // // // // // // // 

	int src1[] = { 2, 3, 4, 6, 8 };
	int src2[] = { 1, 3, 5 };
	int dest[8];
	merge(src1, src1 + 5, src2, src2 + 3, dest);
	for (int j = 0; j < 8; j++) 
		cout << dest[j] << ' ';
	cout << endl;

	// // // // // // // // // // // // // // // // // // // // // // // 

	sort(arr, arr + 9, less<int>());
	for (int i = 0; i < 9; i++)
		cout << arr[i] << ' ';
	cout << endl;

	// // // // // // // // // // // // // // // // // // // // // // // 

	ptr = find_if(arr, arr + 9, is_Numb);
	cout << "������� 45 ��������� �� ������ " << ptr - arr << endl;

	// // // // // // // // // // // // // // // // // // // // // // //

	for_each(arr, arr + 9, multi10);
	cout << endl;
	 
	// // // // // // // // // // // // // // // // // // // // // // //

	int test[9];
	transform(arr, arr + 9, test, dble);
	for (int i = 0; i < 9; i++)
		cout << test[i] << ' ';
	cout << endl;

	// // // // // // // // // // // // // // // // // // // // // // //

	system("pause");
	return 0;
}