#pragma once
#include <iostream>
#include <string.h>

class String
{
private:
	char* str;
	int length;
public:
	String();
	String(const char*);
	void gets();
	void show() const;
	String operator+(const String) const;
	bool operator==(const String) const;

};

String::String() : str(NULL)
{}

String::String(const char *buffer)
{
	length = strlen(buffer);
	str = new char[length + 1];
	strcpy(str, buffer);
}

void String::gets()
{
	char buf[256];
	std::cin.getline(buf, 255);
	length = strlen(buf);
	str = new char[length + 1];
	strcpy(str, buf);
}

void String::show() const
{
	std::cout << str << std::endl;
}

String String::operator+(const String S) const
{
	if (!str)
		return String(S.str);
	if (!S.str)
		return String(str);

	char* buf = new char[length + S.length + 1];
	strcpy(buf, str);
	strcat(buf, S.str);
	return String(buf);
}

bool String::operator==(const String S) const
{
	return !strcmp(str, S.str);
}