#pragma once
#include <iostream>
#include <process.h>
#define parameter 0
class SafeArr
{
private:
	static const int SIZE = 100;
	int arr[SIZE];
public:
#if parameter
	int& access(int n)
	{
		if (n < 0 || n >= SIZE)
		{
			std::cout << "ERROR!" << std::endl;
			exit(1);
		}
		return arr[n];
	}
#else
	int& operator[](int n)
	{
		if (n < 0 || n >= SIZE)
		{
			std::cout << "ERROR!" << std::endl;
			exit(1);
		}
		return arr[n];
	}
#endif
};