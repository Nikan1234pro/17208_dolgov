#include <iostream>
class Line
{
private:
	int *arr;
	int len;
public:
	Line()
	{}

	Line(int l) : len(l)
	{
		arr = new int[len];
		for (int i = 0; i < len; i++)
			arr[i] = 0;
	}

	int& operator[](int i) const
	{
		return arr[i];
	}
};

class MatrixInt
{
private:
	int width;
	int height;
	Line *arr;
public:
	MatrixInt() : width(0), height(0), arr(NULL)
	{}

	MatrixInt(int w, int h) : width(w), height(h)
	{
		arr = new Line [height];

		for(int i = 0; i < height; i++)
			arr[i] = Line(width);
	}

	Line& operator[](int i)
	{
		return arr[i];
	}

};