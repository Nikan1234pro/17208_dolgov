#pragma once
#include <iostream>
class Counter
{
private:
	unsigned int count;
public:
	Counter() : count(0)
	{}

	Counter(int a) : count(a)
	{}

	unsigned int get_count()
	{
		return count;
	}

	Counter operator++() //���������� ++a
	{
		return Counter(++count);
	}

	Counter operator++(int) //���������� �++
	{
		return Counter(count++);
	}

	Counter operator+(Counter C) const //���������� ��������
	{
		return Counter(count + C.count);
	}

	unsigned int* operator&() //���������� �������� ������ ������
	{
		return &count;
	}
	unsigned int operator*() const
	{
		return count;
	}

};