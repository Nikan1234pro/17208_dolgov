#define _CRT_SECURE_NO_WARNINGS
#include <conio.h>
#include "class_1.h"
#include "class_2.h"
#include "class_3.h"
using namespace std;

int main()
{
	// // // // // // // // // // // // // // // // // // //
	Counter c1, c2;
	++c1;
	++c1;
	++c1;
	c2 = c1++;
	cout << c1.get_count() << endl;
	cout << c2.get_count() << endl;

	Counter c3 = c1 + c2;
	cout << c3.get_count() << endl;
	cout << *c3 << endl;
	cout << &c3 << endl;
	
	// // // // // // // // // // // // // // // // // // //
	String s1;
	String s2;
	String s3 = s1 + s2;
	s3.show();
	s2 = "I am Nikita";
	String s4;
	s4.gets();
	if (s1 == s4)
		cout << "s1 and s4 are same!" << endl;
	else
		cout << "You are stupid!" << endl;

	// // // // // // // // // // // // // // // // // // //
	SafeArr arr;
#if parameter
	for (int i = 0; i < 100; i++)
		arr.access(i) = i * 10;

	for (int i = 0; i < 100; i++)
		cout << arr.access(i) << " ";
	cout << endl;
#else
	for (int i = 0; i < 100; i++)
		arr[i] = i * 10;

	for (int i = 0; i < 100; i++)
		cout << arr[i] << " ";
	cout << endl;
#endif
	_getch();
	return 0;
}