#pragma once
#include <iostream>
class Object
{
private:
	static int total;
	int ID;
public:
	Object()
	{
		ID = ++total;
	}

	static void showtotal()
	{
		std::cout << total << std::endl;
	}
	void showid()
	{
		std::cout << "ID: " << ID << std::endl;
	}
	~Object()
	{
		std::cout << "Object " << ID << "was deleted" << std::endl;
	}
};

int Object::total = 0;

/*
Object::showtotal(); // ��� ����������

��� �� �����, ����� ����� �� ���������, ���� showtotal() � ������� �����. 
� ���� ������, �������������, �������� �������� ��� �������, ����� � �������� �������. 
������ ����� ������ � showtotal(), ��������� ������ ���� ��� ������, � ���������� �� �����������. ��� �� � ���������, �������
static void showtotal()
����������� ����� ������� ������� ����� ����� ������ ������ ����� ��� ������.
*/