#pragma once
#include <iostream>
class beta;


class alpha
{
private:
	int a_data;
public:
	alpha() : a_data(37)
	{}
	friend class beta;
};

class beta
{
public:
	void func(alpha a)
	{
		std::cout << a.a_data << std::endl;
	}
};

/*
��� ������ alpha ���� ����� beta ������������ �������������. 
������ ��� ������ beta ����� ������ � ������� ������ ������ alpha 
(� ������ ��������� ��� ���� ������������ ���������� a_data).
*/