#include "main_class.h"

object::object()
{
	id = 0;
	name = "Unknown";
}

object::object(std::string cur_name, int number)
{
	id = number;
	name = cur_name;
}

void object::print()
{
	std::cout << name << " id is: " << id << std::endl;

}