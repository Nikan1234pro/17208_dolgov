/////////////////////////////////
//    Наследование классов     //
/////////////////////////////////

#include <cstdlib>
#include "main_class.h"
int main()
{
	object plane("Plane", 9);
	plane.print();

	cars Car1("Toyota", 7);
	Car1.set_year(1999);
	Car1.print();
	std::cout << Car1.get_year() << std::endl;

	cars Car2;
	Car2.print();
	system("pause");
	return 0;
}