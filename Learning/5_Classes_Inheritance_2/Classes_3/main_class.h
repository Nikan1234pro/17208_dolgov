#pragma once
#include <string>
#include <iostream>

class object
{
	protected:
		std::string name;
		int id;
	public:
		object();
		object(std::string cur_name, int number);
		void print();

};

class cars : public object
{
	public:
		cars() : object()
		{}
		cars(std::string cur_name, int number) :
			object(cur_name, number)
		{}
		void set_year(int num)
		{
			year = num;
		}
		int get_year()
		{
			return year;
		}
	private:
		int year;
};

