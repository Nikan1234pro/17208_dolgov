#pragma once
#include <iostream>
using namespace std;
const int LEN = 80;
class student // �������� �� �����������
{
private:
	char school[LEN]; // �������� �������� ���������
	char degree[LEN]; // ������� �����������
public:
	void getedu()
	{
		cout << " ������� �������� �������� ���������: ";
		cin >> school;
		cout << " ������� ������� ������� �����������\n";
		cout << " (�������� ������, ��������, �������, �������� ����): ";
		cin >> degree;
	}
	void putedu() const
	{
		cout << "\n ������� ���������: " << school;
		cout << "\n �������: " << degree;
	}
};
///////////////////////////////////////////////////////////
class employee // ����� ���������
{
private:
	char name[LEN]; // ��� ����������
	unsigned long number; // ����� ����������
public:
	void getdata()
	{
		cout << "\n ������� �������: "; cin >> name;
		cout << " ������� �����: "; cin >> number;
	}
	void putdata() const
	{
		cout << "\n �������: " << name;
		cout << "\n �����: " << number;
	}
};

class manager : private employee, private student // ��������
{
private:
	char title[LEN]; // ��������� ����������
	double dues; // ����� ������� � �����-����
public:
	void getdata()
	{
		employee::getdata();
		cout << " ������� ���������: "; cin >> title;
		cout << " ������� ����� ������� � �����-����: "; cin >> dues;
		student::getedu();
	}
	void putdata() const
	{
		employee::putdata();
		cout << "\n ���������: " << title;
		cout << "\n ����� ������� � �����-����: " << dues;
		student::putedu();
	}
};