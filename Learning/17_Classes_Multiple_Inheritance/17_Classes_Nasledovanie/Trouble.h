#pragma once
#include <iostream>
class A
{
public:
	void show()
	{
		std::cout << "This is class A" << std::endl;
	}
};

class B
{
public:
	void show()
	{
		std::cout << "This is class B" << std::endl;
	}
};

class C : public A, public B
{};