#pragma once
#include <string>
#include <sstream>

class People
{
	protected:
		std::string name;
		std::string last_name;
	public:
		People();
		People(std::string n, std::string l_n);
		std::string get_name();
};