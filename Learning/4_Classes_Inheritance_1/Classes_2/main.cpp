/////////////////////////////////////////
// ��������� ������ People - Student   //
// ������������� �����������           //
// ���������� �������                  //
/////////////////////////////////////////

#include <iostream>
#include "first_class.h"
#include "second_class.h"

int main()
{
	People dude("Nikita", "Radeev");
	People lol;
	std::cout << dude.get_name();
	std::cout << lol.get_name();

	Student king("Nikita", "Dolgov", 19);
	Student anch;
	std::cout << king.get_name();
	std::cout << anch.get_name();
	system("pause");
	return 0;
}