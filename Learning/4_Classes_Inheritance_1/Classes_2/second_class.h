#pragma once
#include "first_class.h"

class Student : public People
{
	public:
		Student() : People()
		{
			age = 0;
		}
		Student(std::string n, std::string l_n, int a) :
			People(n, l_n), age(0)
		{}
		std::string get_name();
	private:
		int age;
};