#include "first_class.h"
#include "second_class.h"

People::People()
{
	name = "Unknown";
	last_name = "Unknown";
}

People::People(std::string n, std::string l_n)
{
	name = n;
	last_name = l_n;
}

std::string  People::get_name()
{
	std::stringstream buf;
	buf << name << " " << last_name << " - is simply people" << std::endl;
	return buf.str();
}

std::string Student::get_name()
{
	std::stringstream buf;
	buf << name << " " << last_name << " " << age << " is student" << std::endl;
	return buf.str();
}

