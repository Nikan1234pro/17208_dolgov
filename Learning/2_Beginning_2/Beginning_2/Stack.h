#pragma once
#include <iostream>

class Stack
{
private:
	int *st;
	int top;
	int SIZE;
public:
	Stack(int S) : SIZE(S), top(0)
	{
		st = new int[SIZE];
	}

	void push(int value)
	{
		if (top == SIZE)
		{
			std::cout << "Stack is full" << std::endl;
			return;
		}
		else
			st[top++] = value;
	}

	int pop()
	{
		if (!top)
		{
			std::cout << "Stack is empty!" << std::endl;
			return -1;
		}
		return st[--top];
	}

	bool is_empty()
	{
		return (top == 0);
	}
};