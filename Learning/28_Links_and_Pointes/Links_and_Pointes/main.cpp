#include <iostream>
using namespace std;
int add_1(int);
int add_2(int&);
int add_3(int*);
void swap(int &x, int &y) //��� �������� swap � ������� ����� // ����� - swap(a, b); - �������� ������ ����������!
{
	int tmp = x;
	x = y;
	y = tmp;
}

int main()
{
	int value = 15;
	int &link = value;
	link += 10; //������ �������� �� ������
	cout << value << endl;

	cout << "//////////////////////////////" << endl;

	cout << add_1(value);
	cout << " - " << value<< endl;

	cout << add_2(value);
	cout << " - " << value << endl;

	cout << add_3(&value);
	cout << " - " << value << endl;

	system("pause");
	return 0;
}

int add_1(int value)
{
	value += 10;
	return value;
}

int add_2(int &value) // ��������� �� ������
{
	value += 10; 
	return value;
}

int add_3(int *value) // ��������� �� ���������
{
	*value += 10;
	return *value;
}