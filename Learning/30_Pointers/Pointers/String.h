#pragma once
#include <iostream>
#include <cstring>
class String
{
public:
	String();
	String(const char *);
	~String();
	void display() const;
private:
	char *str;
};

String::String() : str()
{}

String::String(const char *ptr)
{
	int len = strlen(ptr);
	str = new char[len + 1];
	strcpy(str, ptr);
}

String::~String()
{
	delete[] str;
	std::cout << "All cleared!" << std::endl;
}

void String::display() const
{
	std::cout << str << std::endl;
}