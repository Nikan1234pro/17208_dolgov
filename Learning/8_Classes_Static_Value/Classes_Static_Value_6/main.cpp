#include <iostream>
using namespace std;
class test
{
	private:
		static int count;

	public:
		test()
		{
			count++;
		}
		int get_number()
		{
			return count;
		}
};
int test::count = 0;

int main()
{
	test f1, f2, f3, f4;
	cout << f1.get_number() << endl;
	cout << f2.get_number() << endl;
	cout << f3.get_number() << endl;
	cout << f4.get_number() << endl;
	system("pause");
	return 0;
}