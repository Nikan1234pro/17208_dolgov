#include <iostream>
#include <list>
using namespace std;

int main()
{
	// 34 23 11 87
	list<int> L;
	L.push_front(23); // ������� ��������� � ������
	L.push_front(34);
	L.push_back(11); // ������� � �����
	L.push_back(87); 

	while (!L.empty())
	{
		cout << L.front() << ' '; // ����� � ������
		L.pop_front();
	}
	cout << endl;

    //	//	//	//	//	//	//	//	//	//	//	//	//	//

	list<int> list1, list2;
	int arr1[] = { 40, 30, 20, 10 };
	int arr2[] = { 15, 20, 25, 30, 35 };
	for (int j = 0; j < 4; j++)
		list1.push_back(arr1[j]); // list1: 40, 30, 20, 10

	for (int j = 0; j < 5; j++)
		list2.push_back(arr2[j]); // list2: 15, 20, 25, 30, 35

	list1.reverse(); // ����������� list1: 10 20 30 40

	list1.merge(list2); // ���������� list2 � list1

	list1.unique(); // ������� ������������� �������� 20 � 30

	int size = list1.size();
	while (!list1.empty())
	{
		cout << list1.front() << ' '; // ������ ������� �� ������
		list1.pop_front(); // ���������� ������� �� ������
	}
	cout << endl;

	system("pause");
	return 0;
}