#include <iostream>
#include <list>
#include <deque>
#include <algorithm>
using namespace std;

const int arr[4] = { 1, 6, 7, 3 };
int main()
{
	list<int> L;
	list<int>::iterator iter;
	list<int>::reverse_iterator riter;
	for (int i = 0; i < 4; i++)
		L.push_back(arr[i]);
	for (iter = L.begin(); iter != L.end(); iter++)
		cout << *iter << ' ';
	cout << endl;

	for (riter = L.rbegin(); riter != L.rend(); riter++)
		cout << *riter << ' ';
	cout << endl;
	int data = 2;
	list<int>L2(5);
	for (iter = L2.begin(); iter != L2.end(); iter++)
		*iter = data += 2;
	for (iter = L2.begin(); iter != L2.end(); iter++)
		cout << *iter << ' ';

	iter = find(L.begin(), L.end(), 7);
	////////////////////////////////////////////////

	int arr1[] = { 1, 3, 5, 7, 9 }; // ������������� d1
	int arr2[] = { 2, 4, 6 }; // ������������� d2
	deque<int> d1;
	deque<int> d2;
	for (int i = 0; i < 5; i++) // ��������� ������ �� �������� �
								// ������� � ������������ ��������
		d1.push_back(arr1[i]);
	for (int j = 0; j < 3; j++)
		d2.push_back(arr2[j]);
	// ���������� d1 � ����� d2
	copy(d1.begin(), d1.end(), back_inserter(d2));
	cout << "\nd2: "; // ������� d2
	for (int k = 0; k < d2.size(); k++)
		cout << d2[k] << ' ';
	cout << endl;


	cin.get();
	return 0;
}

/*
���� �� �� ������� � ���������, ��� �������� ����� ��������� � ������
copy(d1.begin(), d1.end(), front_inserter(d2));
����� ����� �������� ��������� �� ����� ������������� ������� d2. ��������, ������� ����� �� ����, ��� 
������ ����� push_front(), ����������� �������� � ������ � ���������������� ������� �� ����������. � ���� ������ ���������� d2 ���� �� �����:
9 7 5 3 1 2 4 6

����� ��������� ������ � � ������������ ����� ����������, ��������� ������ inserter ���������. ��������, ������� ����� ������ � ������ d2:
copy(d1.begin(), d1.end(), inserter(d2, d2.begin()));
������ ���������� inserter �������� ��� ����������, � ������� ����� ����- ������ ��������, ������ � ��������, ����������� �������, 
������� � ������� ������ ����������� ������. ��� ��� inserter ���������� ����� insert(), �� ������� ���������� ��������� �� 
����������. �������������� ��������� ����� ��������� ��������� ������:
1 3 5 7 9 2 4 6
*/