import java.util.Scanner;

class Main
{
	public static void main(String[] args)
	{
		Types tp = new Types();
		tp.show();
	}
}

class Types
{
	private byte b;  				// 1 byte -128 - 127  
	private short sh;  			 	// 2 byte
	private int i;					// 4 byte
	private float f;				// 4 byte, must have 'f'
	private double d;				// 8 byte
	private char ch = 'f';			// 1 byte - symbols
	private long l;					// 8 byte
	private boolean t;	
	String str;
	
	Types()
	{
		System.out.println("Input data: ");
		Scanner in = new Scanner(System.in);
		b = in.nextByte();
		sh = in.nextShort();
		i = in.nextInt();
		f = in.nextFloat();
		d = in.nextDouble();
		l = in.nextLong();
		t = in.nextBoolean();
		in.nextLine();    			//ignore '\n'
		str = in.nextLine();
	}
	
	public void show()
	{
		System.out.println("Byte = " + b);
		System.out.printf("short = %d\n", sh); 
		System.out.println("int = " + i);
		System.out.println("float = " + f);
		System.out.println("double" + d);
		System.out.printf("char = %c\n", ch);
		System.out.println("line = " + str);
	}
	
}