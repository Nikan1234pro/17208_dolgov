class Main 
{
	public static void main(String[] args)
	{
		
		Human Nick = new Human();
		Nick.Name();
		Nick.LastName();
		Human.increment();
		Human.increment();
		System.out.println(Human.GetCount());
		System.out.println("Hello, world");
	}
}


class Human
{
	private static int count;
	public Human()
	{
		count = 0;
	}
	
	public void Name()
	{
		System.out.println("Nikita");
	}
	
	public void LastName()
	{
		System.out.println("Dolgov");
	}
	
	public static void increment()
	{
		count++;
	}
	
	public static int GetCount()
	{
		return count;
	}
}