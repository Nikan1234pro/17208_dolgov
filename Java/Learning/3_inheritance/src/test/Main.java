package test;
import Inheritance.Person;
import Inheritance.Student;

public class Main {

	public static void main(String[] args) 
	{
		Person pers = new Person(19, "Nick");
		pers.say();
		
		System.out.println('\n');
		
		Student dude = new Student();
		dude.SetSchool("NSU");
		dude.say();
		
		System.out.println('\n');
		
		Person w = new Student();
		w.say();   // call Student method!?
	}

}
