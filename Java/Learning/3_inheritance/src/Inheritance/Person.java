package Inheritance;

public class Person 
{
	private int age;
	private String name;
	public Person()
	{
		age = 0;
		name = "unknown";
	}
	
	public Person(int age, String name)
	{
		this.age = age;
		this.name = name;
	}
	
	public void SetAge(int age)
	{
		this.age = age;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void say() 
	{
		System.out.println("Hello, I am " + name + ". I am " + age + " years old");
	}
}

