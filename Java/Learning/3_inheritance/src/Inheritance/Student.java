package Inheritance;

public class Student extends Person
{
	private String school;
	public Student()
	{
		super();
		school = "unknown";
	}
	
	public Student(int age, String name, String school)
	{
		super(age, name);
		this.school = school;
	}
	
	public void SetSchool(String school)
	{
		this.school = school;
	}
	
	public void say()
	{
		super.say();
		System.out.println("I am studying in " + school);
	}
}
