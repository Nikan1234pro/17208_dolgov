package LaboratoryWork;
import java.lang.StringBuilder;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.*;

public class WordCounter 
{
	private int total = 0;
	private Map<String, Integer> words;
	
	public WordCounter(String in)
	{
		Reader reader = null;
		words = new HashMap<String, Integer>();
		try 
		{
			reader = new InputStreamReader(new FileInputStream(in));
			StringBuilder builder = new StringBuilder();
			int data = reader.read();
			while(data != -1)
			{
				if(Character.isLetterOrDigit(data))
					builder.append((char)data);
				else
				{
					if (builder.length() != 0)
					{
						total++;
						String word = builder.toString();
						words.put(word, (words.containsKey(word)) ? words.get(word) + 1 : 1);
						builder.setLength(0);
					}
				}
				data = reader.read();
			}
		}
		catch(IOException ex)
		{
			System.err.println("Error: " + ex.getLocalizedMessage());
		}
		finally
		{
			if (reader != null)
			{
				try
				{
					reader.close();
				}
				catch(IOException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
	}
	
	
	public void Print()
	{
		Map<String, Integer> OutMap = Sort(words);
		for (Entry<String, Integer> pair : OutMap.entrySet())
			System.out.printf("%s;%d;%f;\n", pair.getKey(), pair.getValue(), (double)pair.getValue() / total * 100);
	}
	
	
	private <K, V extends Comparable<V> > Map<K, V> Sort(Map<K, V> in)
	{
		Map<K, V> out = new LinkedHashMap<>();
		ArrayList<Entry<K, V>> list = new ArrayList<Entry<K, V>>(in.entrySet());
		list.sort(Collections.reverseOrder(Entry.comparingByValue()));
		for (Entry<K, V> pair : list)
			out.put(pair.getKey(), pair.getValue());
		return out;
	}
}
