#pragma once
#include <fstream>
#include <vector>
#include <tuple>
#include <string>

template <typename... Args>
class CSVParser
{
private:
	char row_delim;
	char col_delim;
	char escape;
	std::vector< std::tuple<Args...> > data;

	std::vector<std::string> parse(const std::string &str)
	{
		std::vector<std::string> line;
		std::string buf;
		bool end = false;
		for (int i = 0; i < str.size(); i++)
		{
			char symbol = str[i];
			if (symbol == escape)
			{
				end = !end;
			}
			if (i = col_delim || end)
			{
				line.push_back(buf);
				buf = "";
			}
			else
				buf += symbol;
		}
		return line;
	}

public:
	CSVParser(std::ifstream &file, int skip = 0, char row_delim = '\n', char col_delim = ';', char escape = '"')
		: row_delim(row_delim), col_delim(col_delim), escape(escape)
	{
		while (skip)
		{
			std::string tmp;
			std::getline(file, tmp, row_delim);
			skip--;
		}

		while (!file.eof())
		{
			std::string str;
			std::getline(file, str, row_delim);
			auto line = parse(str);
			for (int i = 0; i < line.size(); i++)
				std::cout << line[i] << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	}
};