#pragma once
#include <ostream>
#include <tuple>

namespace tpl
{
	template <typename Stream, typename Type, size_t Cur, size_t Last>
	struct tuple_printer
	{
		static void print(Stream &os, const Type &t)
		{
			os << std::get<Cur>(t) << "; ";
			tuple_printer<Stream, Type, Cur + 1, Last>::print(os, t);
		}
	};

	template <typename Stream, typename Type, size_t Last>
	struct tuple_printer<Stream, Type, Last, Last>
	{
		static void print(Stream &os, const Type &t)
		{
			os << std::get<Last>(t);
		}
	};
}



template <typename Ch, typename Tr, typename... Args>
auto& operator<<(std::basic_ostream<Ch, Tr>& os, std::tuple<Args...> const& t)
{
	os << '(';
	tpl::tuple_printer<std::basic_ostream<Ch, Tr>, std::tuple<Args...>, 0, sizeof...(Args) - 1>::print(os, t);
	os << ')';
	return os;
}

