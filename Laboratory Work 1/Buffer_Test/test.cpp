#include "pch.h"
#include "gtest/gtest.h"
#include "../Lab_1_CircularBuffer/buffer.h"
#include <iostream>
#include <string>

template <class T>
void show(const CircularBuffer<T> &a)
{
	for (int i = 0; i < a.size(); i++)
		std::cout << std::setw(4) << std::setiosflags(std::ios::left) << a.at(i);
	std::cout << std::endl << std::endl;
}

TEST(TEST_1, Constructors)
{
	ASSERT_NO_THROW(CircularBuffer<int> Buffer);

	ASSERT_THROW(CircularBuffer<int> Buffer(-1), std::bad_alloc);
	ASSERT_NO_THROW(CircularBuffer<std::string> Buffer(5));

	ASSERT_THROW(CircularBuffer<double> Buffer(-1, 5.8), std::bad_alloc);
	ASSERT_NO_THROW(CircularBuffer<std::string> Buffer(5, "adc"));

	CircularBuffer<std::string> Buffer_copy_1(5, "adc");
	CircularBuffer<std::string> Buffer_copy_2;
	ASSERT_NO_THROW(CircularBuffer<std::string> Buffer(Buffer_copy_1));
	ASSERT_NO_THROW(CircularBuffer<std::string> Buffer(Buffer_copy_2));
	CircularBuffer<std::string> Buffer_new(Buffer_copy_1);
	ASSERT_EQ(Buffer_new.size(), Buffer_copy_1.size());
	ASSERT_EQ(Buffer_new.capacity(), Buffer_copy_1.capacity());
	for (int i = 0; i < Buffer_new.size(); i++)
		ASSERT_EQ(Buffer_new.at(i), Buffer_copy_1.at(i));
	
}

TEST(TEST_2, Buffer_Parameters_Funcs)
{
	CircularBuffer<int> Buffer0;
	ASSERT_EQ(0, Buffer0.size());
	ASSERT_EQ(0, Buffer0.reserve());
	ASSERT_EQ(0, Buffer0.capacity());
	ASSERT_FALSE(Buffer0.empty());
	ASSERT_TRUE(Buffer0.full());

	CircularBuffer<int> Buffer1(10);
	ASSERT_EQ(0, Buffer1.size());
	Buffer1.push_back(2);
	ASSERT_EQ(1, Buffer1.size());
	ASSERT_EQ(9, Buffer1.reserve());
	ASSERT_EQ(10, Buffer1. capacity());

	CircularBuffer<std::string> Buffer2(5, "Nikita");
	ASSERT_EQ(5, Buffer2.size());
	ASSERT_EQ(0, Buffer2.reserve());
	ASSERT_EQ(5, Buffer2.capacity());
	Buffer2.push_front("Dolgov");
	ASSERT_EQ(5, Buffer2.size());
	ASSERT_TRUE(Buffer2.full());
}

TEST(TEST_3, Swap)
{
	CircularBuffer<std::string> Bf1(10, "LOL");
	CircularBuffer<std::string> Bf2(3);
	Bf1.swap(Bf2);
	ASSERT_EQ(3, Bf1.capacity());
	ASSERT_FALSE(Bf1.full());

	for (int i = 0; i < Bf2.size(); i++)
		ASSERT_EQ(Bf2[i], "LOL");
}

TEST(TEST_4, Resize_of_Buffer)
{
	CircularBuffer<std::string> Buffer;
	ASSERT_THROW(Buffer.set_capacity(0), std::bad_alloc);
	ASSERT_NO_THROW(Buffer.set_capacity(7));
	ASSERT_EQ(7, Buffer.capacity());
	ASSERT_TRUE(Buffer.empty());
	
	CircularBuffer<int> Buffer_new(10, 9);
	ASSERT_NO_THROW(Buffer_new.set_capacity(6));
	ASSERT_EQ(Buffer_new.size(), 6);
	ASSERT_EQ(Buffer_new.capacity(), 6);
	ASSERT_TRUE(Buffer_new.full());
	for (int i = 0; i < Buffer_new.size(); i++)
		ASSERT_EQ(Buffer_new[i], 9);

	ASSERT_THROW(Buffer_new.resize(-3), std::bad_alloc);

	ASSERT_NO_THROW(Buffer_new.resize(9, 0));
	ASSERT_EQ(Buffer_new.capacity(), 9);
	ASSERT_EQ(Buffer_new.size(), 9);
	ASSERT_TRUE(Buffer_new.full());
	for (int i = 0; i < Buffer_new.size(); i++)
	{
		if (i < 6)
			ASSERT_EQ(Buffer_new[i], 9);
		else
			ASSERT_EQ(Buffer_new[i], 0);
	}
}

TEST(TEST_5, Operators)
{
	CircularBuffer<std::string> Buffer_str_1;
	CircularBuffer<std::string> Buffer_str_2(10, "dd");
	CircularBuffer<std::string> Buffer_str_3(5);
	CircularBuffer<int> Buffer1(3, 7);
	CircularBuffer<int> Buffer2;
	Buffer2 = Buffer1;
	ASSERT_EQ(Buffer1.size(), Buffer2.size());
	ASSERT_EQ(Buffer1.capacity(), Buffer2.capacity());
	for (int i = 0; i < Buffer1.size(); i++)
		ASSERT_EQ(Buffer1[i], Buffer2[i]);

	Buffer_str_3 = Buffer_str_2 = Buffer_str_1;
	ASSERT_EQ(Buffer_str_2.size(), 0);
	ASSERT_EQ(Buffer_str_3.size(), 0);
	ASSERT_EQ(Buffer_str_2.capacity(), 0);
	ASSERT_EQ(Buffer_str_3.capacity(), 0);

	ASSERT_THROW(Buffer2.at(3), std::out_of_range);
	ASSERT_EQ(Buffer2.at(0), 7);

	ASSERT_NO_THROW(Buffer2[3]);
	ASSERT_EQ(Buffer2[0], 7);

	ASSERT_THROW(Buffer_str_1.front(), std::out_of_range);
	ASSERT_THROW(Buffer_str_1.back(), std::out_of_range);

	CircularBuffer<std::string> B_new;
	B_new.set_capacity(3);
	B_new.push_back("aa");
	
	ASSERT_EQ(B_new.back(), "aa");
	ASSERT_EQ(B_new.front(), "aa");
	B_new.push_front("bcf");
	ASSERT_EQ(B_new.front(), "bcf");
	ASSERT_EQ(B_new.back(), "aa");
}

TEST(TEST_6, Push_Pop_Rotate_Clear)
{
	CircularBuffer<int> empty_bf;
	ASSERT_NO_THROW(empty_bf.clear());
	ASSERT_THROW(empty_bf.push_back(), std::out_of_range);
	ASSERT_THROW(empty_bf.push_front(), std::out_of_range);
	ASSERT_THROW(empty_bf.rotate(6), std::out_of_range);

	CircularBuffer<int> Buffer1(3);
	for (int i = 0; i < 10; i++)
		ASSERT_NO_THROW(Buffer1.push_back(i * 10));
	ASSERT_EQ(Buffer1[0], 70);
	ASSERT_EQ(Buffer1[1], 80);
	ASSERT_EQ(Buffer1[2], 90);
	ASSERT_NO_THROW(Buffer1.clear());

	for (int i = 0; i < 10; i++)
		ASSERT_NO_THROW(Buffer1.push_front(i * 2));
	ASSERT_EQ(Buffer1[0], 18);
	ASSERT_EQ(Buffer1[1], 16);
	ASSERT_EQ(Buffer1[2], 14);

	ASSERT_THROW(Buffer1.rotate(3), std::out_of_range);
	ASSERT_THROW(empty_bf.rotate(0), std::out_of_range);

	Buffer1.set_capacity(8);
	ASSERT_NO_THROW(Buffer1.rotate(0));
	ASSERT_NO_THROW(Buffer1.rotate(2));
	ASSERT_EQ(Buffer1[0], 14);
	ASSERT_EQ(Buffer1[1], 18);
	ASSERT_EQ(Buffer1[2], 16);
	ASSERT_NO_THROW(Buffer1.rotate(1));

	ASSERT_EQ(Buffer1.size(), 3);
	for (int i = 9; i > 6; i--)
	{
		ASSERT_EQ(Buffer1.front(), i * 2);
		Buffer1.pop_front();
	}
	ASSERT_EQ(Buffer1.size(), 0);


	for (int i = 0; i < 8; i++)
		Buffer1.push_front(i);

	for (int i = 0; i < 8; i++)
	{
		ASSERT_EQ(Buffer1.back(), i);
		Buffer1.pop_back();
	}
	ASSERT_EQ(Buffer1.size(), 0);

	CircularBuffer<int> Buffer2(2);
	Buffer2.push_back(1);
	Buffer2.push_back(2);
	ASSERT_NO_THROW(Buffer2.rotate(1));
	ASSERT_EQ(2, Buffer2.front());
	ASSERT_EQ(1, Buffer2.back());

}

TEST(TEST_7, Insert_Erase)
{
	CircularBuffer<double> Buffer;
	ASSERT_NO_THROW(Buffer.set_capacity(6));
	ASSERT_THROW(Buffer.insert(1, 34.78), std::out_of_range);
	ASSERT_NO_THROW(Buffer.insert(0, 34.7));
	ASSERT_EQ(Buffer.front(), Buffer.back());
	ASSERT_EQ(Buffer[0], 34.7);

	for (int i = 0; i < 6; i++)
	{
		Buffer.push_back(i);
	}
	ASSERT_THROW(Buffer.erase(0, 6), std::out_of_range);
	ASSERT_NO_THROW(Buffer.erase(0, 0));
	ASSERT_NO_THROW(Buffer.erase(4, 4));
	ASSERT_EQ(Buffer.front(), 1);
	ASSERT_EQ(Buffer.back(), 4);
	ASSERT_NO_THROW(Buffer.insert(4, 8.67));
	ASSERT_NO_THROW(Buffer.insert(1, 234.7));
	ASSERT_EQ(Buffer[5], 8.67);
	ASSERT_EQ(Buffer[1], 234.7);
	ASSERT_EQ(Buffer.size(), 6);

	ASSERT_NO_THROW(Buffer.erase(0, 4));
	ASSERT_EQ(Buffer.size(), 1);
	ASSERT_EQ(Buffer.front(), 8.67);
	ASSERT_NO_THROW(Buffer.erase(0, 0));
	ASSERT_EQ(Buffer.size(), 0);
	ASSERT_THROW(Buffer.front(), std::out_of_range);
}

TEST(TEST_8, Linear_opts)
{
	CircularBuffer<std::string> Buffer(10);
	for (int i = 0; i < 12; i++)
		Buffer.push_back("TEST");
	ASSERT_FALSE(Buffer.is_linearized());
	std::string *temp = new std::string[10];
	temp = Buffer.linearize();
	for (int i = 0; i < 10; i++)
		ASSERT_EQ(temp[i], Buffer[i]);

	CircularBuffer<int> Empty;
	ASSERT_THROW(Empty.linearize(), std::out_of_range);
}

TEST(TEST_9, Destructor)
{
	CircularBuffer<double> *ptrBuf1 = new CircularBuffer<double>;
	CircularBuffer<double> *ptrBuf2 = new CircularBuffer<double>(7, 34.56);
	ASSERT_NO_THROW(delete ptrBuf1);
	ASSERT_NO_THROW(delete ptrBuf2);
}

TEST(TEST_0, —omparison_Operators)
{
	CircularBuffer<int> Buffer1(3, 7);
	CircularBuffer<int> Buffer2;
	Buffer2.resize(3, 7);
	ASSERT_TRUE(Buffer1 == Buffer2);
	Buffer2.at(0) = 5;
	ASSERT_TRUE(Buffer1 != Buffer2);

	Buffer1.push_back(7);
	Buffer1.insert(2, 4);
	Buffer2.push_back(4);
	ASSERT_TRUE(Buffer1 == Buffer2);
}