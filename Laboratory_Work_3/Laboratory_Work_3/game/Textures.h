#pragma once
#include "../gui/base_game_window.h"
#include "../parameters.h"
#include <map>
#include <string>
#include <vector>
class Textures
{
private:
	std::map<std::string, std::vector< gdi::Image*> > textures;
	Textures();
public:
	static Textures* GetInstance();
	std::vector<gdi::Image *> GetTexture(std::string name) const;
	~Textures();
};


