#include "Textures.h"
#include <iostream>
#include <sstream>

Textures::Textures()
{
	gdi::Image *image;
	for (int i = 0; i < 20; i++)
	{
		std::wstringstream name;
		name << "game/textures/wall" << i << ".png";
		image = new gdi::Image(name.str().c_str(), true);
		textures["walls"].push_back(image);
	}

	for (int i = 0; i < 9; i++)
	{
		std::wstringstream name;
		name << "game/textures/destruction" << i << ".png";
		image = new gdi::Image(name.str().c_str(), true);
		textures["destructions"].push_back(image);
	}

	image = new gdi::Image(L"game/textures/background.png", true);
	textures["background"].push_back(image);

	image = new gdi::Image(L"game/textures/TNT.png", true);
	textures["TNT"].push_back(image);
}

Textures* Textures::GetInstance()
{
	static Textures *instance = new Textures;
	return instance;
}

std::vector<gdi::Image *> Textures::GetTexture(std::string name) const
{
	auto it = textures.find(name);
	if (it == textures.end())
		throw std::out_of_range("No Textures");
	return it->second;
}

Textures::~Textures()
{
	for (auto it1 = textures.begin(); it1 != textures.end(); it1++)
		for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
			delete *it2;
}
