#include <cstdlib>
#include <ctime>
#include <cmath>
#include "Animations.h"
#include "Field.h"
#include "Shapes.h"

Field::Field(int w, int h) : Width(w), Height(h)
{
	Space = Width / 20;											//������� ������ ����������� ����
	CountOfWalls = Width * 2;									//����� ����

	Matrix.resize(Width);
	for (int i = 0; i < Width; i++)
		Matrix[i].resize(Height);

	for (int i = 0; i < CountOfWalls; i++)
	{
		int x = std::rand() % (Width - 2 * Space) + Space;		//���������� (x, y) ����� ������� ������� �����
		int y = std::rand() % (Height);
		int w = std::rand() % 10;								//������ � ������ �����
		int h = std::rand() % 10;

		if (y + h >= Height)									//���� ����� �������� �� ������� ���� - �������� ������
			h = Height - y;

		if (x + w >= Width - Space)								//���� ����� �������� �� ����������� ����� ���� - �������� ����� ����� �� �������
			w = Width - Space - x;

		for (int j = 0; j < w; j++)
			for (int k = 0; k < h; k++)
				if (!Matrix[x + j][y + k])
				{
					int parameter = std::rand() % 1000;  //������� �����
					if (parameter % 20)
						Matrix[x + j][y + k] = new Rect;		
					else
						Matrix[x + j][y + k] = new SuperRect;
				}
					
	}
}

void Field::Draw(gdi::Graphics &canvas) const
{
	for (int i = 0; i < Width; i++)
		for (int j = 0; j < Height; j++)
		{
			const Vector_2D vertex(i * texture_size, j * texture_size);
			if (Matrix[i][j])
				Matrix[i][j]->Draw(canvas, vertex);
		}
}

int Field::Destroy(int i, int j)
{
	if (!Matrix[i][j])
		return 0;
	int count = 0;
	if (isSuperRect(Matrix[i][j]))
	{
		delete Matrix[i][j];
		Matrix[i][j] = NULL;
		for (int a = i - 10; a <= i + 10; a++)
			for (int b = j - 10; b <= j + 10; b++)
			{
				if (a < 0 || a >= Width)
					break;
				if (b < 2 || b >= Height)
					continue;
				count += Field::Destroy(a, b);
			}
	}
	count++;
	delete Matrix[i][j];
	Matrix[i][j] = NULL;
	float x = i * texture_size;
	float y = j * texture_size;
	Animation_Manager::GetInstance()->Add<Destruction>(x, y);
	return count;
}

std::vector <Shapes*>& Field::operator[](int i)
{
	return Matrix[i];
}

const std::vector <Shapes*>& Field::operator[](int i) const
{
	return Matrix[i];
}

int Field::GetWidth() const
{
	return Width;
}

int Field::GetHeight() const
{
	return Height;
}

int Field::GetCount() const
{
	return Shapes::GetCount();
}

Field::~Field()
{
	for (int i = 0; i < Width; i++)
		for (int j = 0; j < Height; j++)
			delete Matrix[i][j];
}
