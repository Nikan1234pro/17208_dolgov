#include "arkanoid.h"
#include "Animations.h"

Arkanoid::Arkanoid(int w, int h, int _screen_w, int _screen_h) : screen_w(_screen_w), screen_h(_screen_h), field(w, h), 
																 player(texture_size/ 2, (_screen_h + head) / 2, gdi::Color::Red),
																 bot(_screen_w - texture_size / 2, (_screen_h + head)/ 2, gdi::Color::Blue)
{}

void Arkanoid::Draw(gdi::Graphics &canvas) 
{
	Animation_Manager::GetInstance()->Draw(canvas);
	field.Draw(canvas);
	player.Draw(canvas, screen_w, screen_h, field);
	bot.Draw(canvas, screen_w, screen_h);
	DrawScore(canvas, screen_w, screen_h, player, bot);
	
}

void Arkanoid::UpdateWorld(float dt)
{
	float dt2 = 0.0f;
	for (; dt2 < dt; dt2 += 0.05f)
	{
		Animation_Manager::GetInstance()->Update(0.05f);
		player.Update(0.05f, field, screen_w, screen_h);
		bot.Update(0.05f, field, screen_w, screen_h);
		Update(player, bot);
	}
	Animation_Manager::GetInstance()->Update(dt2 - dt);
	player.Update(dt2 - dt, field, screen_w, screen_h);
	bot.Update(dt2 - dt, field, screen_w, screen_h);
	Update(player, bot);
}

void Arkanoid::ProcessInput(const bool keys[256])
{
	player.ProcessInput(keys);
}