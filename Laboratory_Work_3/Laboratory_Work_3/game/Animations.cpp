#include "Animations.h"
#include "Textures.h"

Animation::Animation(float _timeline, float _count) :
	timeline(_timeline), time(0), count_of_frames(_count)
{}

bool Animation::Update(float dt)
{
	time += dt;
	if (time > timeline)
		return true;
	return false;
}


Destruction::Destruction(float _x, float _y) : x(_x), y(_y), Animation(1.5f, 9)
{}


void Destruction::Draw(gdi::Graphics &canvas) const
{
	int cur_frame = (time / timeline) * count_of_frames;
	gdi::Image *image = Textures::GetInstance()->GetTexture("destructions")[cur_frame];
	float a = texture_size;
	canvas.DrawImage(image, x, y, a, a);
}


Animation_Manager::Animation_Manager()
{
	animations.clear();
}

Animation_Manager* Animation_Manager::GetInstance()
{
	static Animation_Manager *instance = new Animation_Manager;
	return instance;
}

void Animation_Manager::Update(float dt)
{
	for (auto it = animations.begin(); it != animations.end();	)
	{
		bool is_completed = (*it)->Update(dt);
		if (is_completed)
		{
			delete *it;
			it = animations.erase(it);
		}
		else
			it++;
	}
}

void Animation_Manager::Draw(gdi::Graphics &canvas) const
{
	for (auto *anim : animations)
		anim->Draw(canvas);
}

Animation_Manager::~Animation_Manager()
{}