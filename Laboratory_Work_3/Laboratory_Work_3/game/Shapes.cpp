#include "Field.h"
#include "Shapes.h"
#include "../Vector_2D.h"
#include <dos.h>
#include "Textures.h"
#include "Animations.h"

int Shapes::count = 0;

int Shapes::GetCount()
{
	return count;
}

Rect::Rect() : a(texture_size), color(std::rand() % 20)
{
	count++;
}

void Rect::Draw(gdi::Graphics &canvas, const Vector_2D &vertex) const
{
	gdi::Image *image = Textures::GetInstance()->GetTexture("walls")[color];
	float a = texture_size;
	canvas.DrawImage(image, vertex.Get_x(), vertex.Get_y(), a, a);
}

Rect::~Rect()
{
	count--;
}

SuperRect::SuperRect() : a(texture_size)
{
	count++;
}

void SuperRect::Draw(gdi::Graphics &canvas, const Vector_2D &vertex) const
{
	gdi::Image *image = Textures::GetInstance()->GetTexture("TNT")[0];
	float a = texture_size;
	canvas.DrawImage(image, vertex.Get_x(), vertex.Get_y(), a, a);
}

SuperRect::~SuperRect()
{
	count--;
}

bool isSuperRect(Shapes *ptr)
{
	if (!ptr)
		return false;
	if (typeid(*ptr) == typeid(SuperRect))
		return true;
	return false;
}

Ball::Ball(float x, float y, gdi::Color color) : center(x, y), speed(0.0f, 0.0f), r(texture_size / 2), base(x, y), brush(color)
{}

void Ball::Draw(gdi::Graphics &canvas) const
{
	canvas.FillEllipse(&brush, center.Get_x() - r, center.Get_y() - r, 2 * r, 2 * r);
}

void Ball::SetSpeed(float x, float y)
{
	if (OnBase())
	{
		Vector_2D Cursor(x, y);
		speed.Get_x() = v * Cos(center, Cursor);
		speed.Get_y() = v * Sin(center, Cursor);
	}
}

void Ball::SetSpeed(Vector_2D &Cursor)
{
	if (OnBase())
	{
		speed.Get_x() = v * Cos(center, Cursor);
		speed.Get_y() = v * Sin(center, Cursor);
	}
}

void Ball::SetCenter()
{
	center = base;
	speed = Vector_2D{};
}

void Ball::Update(float dt, int screen_w, int screen_h)
{
	center = center + speed * dt;

	Vector_2D A(r, r + head);
	Vector_2D B(r, screen_h - r);
	Vector_2D C(screen_w - r, screen_h - r);
	Vector_2D D(screen_w - r, r + head);

	do
	{
		if (center.Get_x() < A.Get_x())
		{
			center = center.FindIntersection(speed, A, B);
			speed = speed.Reflection(Vector_2D(r, 0.0f));
		}
		if (center.Get_x() > C.Get_x())
		{
			center = center.FindIntersection(speed, C, D);
			speed = speed.Reflection(Vector_2D(-r, 0.0f));
		}
		if (center.Get_y() < D.Get_y())
		{
			center = center.FindIntersection(speed, A, D);
			speed = speed.Reflection(Vector_2D(0.0f, r));
		}
		if (center.Get_y() > B.Get_y())
		{
			center = center.FindIntersection(speed, B, C);
			speed = speed.Reflection(Vector_2D(0.0f, -r));
		}
	} while (!center);

	if (center.Get_x() == base.Get_x())
		speed = Vector_2D{};
}

bool Ball::IsInsideRect(const Vector_2D &point, const Vector_2D &Rect) const
{
	float a = texture_size;
	if ((point.Get_x() > Rect.Get_x() - r) && (point.Get_x() < Rect.Get_x() + a + r) &&
		(point.Get_y() > Rect.Get_y()) && (point.Get_y() < Rect.Get_y() + a))
		return true;
	if ((point.Get_x() > Rect.Get_x()) && (point.Get_x() < Rect.Get_x() + a) &&
		(point.Get_y() > Rect.Get_y() - r) && (point.Get_y() < Rect.Get_y() + a + r))
		return true;
	if (point.GetDistance(Rect.Get_x(), Rect.Get_y()) < r)
		return true;
	if (point.GetDistance(Rect.Get_x(), Rect.Get_y() + a) < r)
		return true;
	if (point.GetDistance(Rect.Get_x() + a, Rect.Get_y()) < r)
		return true;
	if (point.GetDistance(Rect.Get_x() + a, Rect.Get_y() + a) < r)
		return true;
	return false;
}

bool Ball::OnBase() const
{
	if (center.Get_x() == base.Get_x())
		return true;
	return false;
}

//����� ���������� ���������� ��������, � ������� ��������� collision � ���������� ������ ����
std::pair<Vector_2D, Vector_2D> Ball::GetCollisionRect(std::vector<Vector_2D> &vect)
{
	if (vect.empty())
		return std::make_pair(Vector_2D{}, Vector_2D{});

	float min_dist = 10000;
	Vector_2D point[4];
	Vector_2D min_vert;
	Vector_2D collision_place;
	for (auto iter1 = vect.begin(); iter1 != vect.end(); iter1++)
	{
		if (IsInsideRect(center, *iter1))
		{
			float x1 = iter1->Get_x();
			float y1 = iter1->Get_y();
			point[0] = center.FindIntersection(speed, Vector_2D(x1 - r, y1 - r),
				Vector_2D(x1 - r, y1 + texture_size + r));
			point[1] = center.FindIntersection(speed, Vector_2D(x1 - r, y1 + texture_size + r),
				Vector_2D(x1 + texture_size + r, y1 + texture_size + r));
			point[2] = center.FindIntersection(speed, Vector_2D(x1 + texture_size + r, y1 + texture_size + r),
				Vector_2D(x1 + texture_size + r, y1 - r));
			point[3] = center.FindIntersection(speed, Vector_2D(x1 + texture_size + r, y1 - r),
				Vector_2D(x1 - r, y1 - r));
		}
		for (int i = 0; i < 4; i++) //�������� �� ��, ��� ��� ������ ������ �� ����������
		{
			if (point[i])
			{
				auto iter2 = vect.begin();
				for (; iter2 != vect.end(); iter2++)
				{
					if (IsInsideRect(point[i], *iter2))
						break;
				}
				if (iter2 != vect.end())
					break;
				for (iter2 = vect.begin(); iter2 != vect.end(); iter2++)
				{
					float x1 = iter2->Get_x();
					float y2 = iter2->Get_y();
					float dist = point[i].GetDistance(Vector_2D(x1 + texture_size / 2, y2 + texture_size / 2));
					if (dist < min_dist)
					{
						min_dist = dist;
						min_vert = Vector_2D(x1, y2);
						collision_place = point[i];
					}
				}
			}
		}
	}
	return std::make_pair(min_vert, collision_place);
}

//������� ����������� ����
int Ball::GetPhysics(Field &field)
{
	float x;
	float y;
	std::vector<Vector_2D> possible_collisions;
	int a = center.Get_x() / texture_size;
	int b = center.Get_y() / texture_size;
	for (int i = a - 1; i <= a + 1; i++)
	{
		for (int j = b - 1; j <= b + 1; j++)
		{
			if (i < 0 || i >= field.GetWidth())
				break;
			if (j < 0 || j >= field.GetHeight())
				continue;
			if (field[i][j])
			{
				x = static_cast<float>(i * texture_size);
				y = static_cast<float>(j * texture_size);
				if (IsInsideRect(center, Vector_2D(x, y)))
					possible_collisions.push_back(Vector_2D(x, y));
			}
		}
	}
	auto data = GetCollisionRect(possible_collisions);
	Vector_2D my_rect = data.first;
	Vector_2D collision_place = data.second;
	
	if (my_rect)
	{
		x = my_rect.Get_x();
		y = my_rect.Get_y();
		int i = static_cast<int>(x / texture_size);
		int j = static_cast<int>(y / texture_size);
		
		int count = field.Destroy(i, j);
		std::cout << count << std::endl;
		Vector_2D A1(x - r, y);
		Vector_2D A2(x, y - r);
		Vector_2D B1(x - r, y + texture_size);
		Vector_2D B2(x, y + texture_size + r);
		Vector_2D C1(x + texture_size + r, y);
		Vector_2D C2(x + texture_size, y - r);
		Vector_2D D1(x + texture_size + r, y + texture_size);
		Vector_2D D2(x + texture_size + r, y + texture_size);
		if (collision_place)
		{
			if (collision_place.IsInsideRect(A1, A2))
			{
				center = collision_place.MoveToArc(speed, A1, A2, Vector_2D(x, y));
				speed = speed.Reflection(center, Vector_2D(x, y));
				return count;
			}
			if (collision_place.IsInsideRect(B1, B2))
			{
				center = collision_place.MoveToArc(speed, B1, B2, Vector_2D(x, y + texture_size));
				speed = speed.Reflection(center, Vector_2D(x, y + texture_size));
				return count;
			}
			if (collision_place.IsInsideRect(C1, C2))
			{
				center = collision_place.MoveToArc(speed, C1, C2, Vector_2D(x + texture_size, y));
				speed = speed.Reflection(center, Vector_2D(x + texture_size, y));
				return count;
			}
			if (collision_place.IsInsideRect(D1, D2))
			{
				center = collision_place.MoveToArc(speed, D1, D2, Vector_2D(x + texture_size, y + texture_size));
				speed = speed.Reflection(center, Vector_2D(x + texture_size, y + texture_size));
				return count;
			}
			center = collision_place;
			if (center.Get_x() + r == x)
				speed = speed.Reflection(Vector_2D(-r, 0.0f));
			if (center.Get_x() - r == x + texture_size)
				speed = speed.Reflection(Vector_2D(r, 0.0f));
			if (center.Get_y() + r == y)
				speed = speed.Reflection(Vector_2D(0.0f, -r));
			if (center.Get_y() - r == y + texture_size)
				speed = speed.Reflection(Vector_2D(0.0f, r));
			return count;
		}
	}
	return 0;
}

const float& Ball::Get_x() const
{
	return center.Get_x();
}

const float& Ball::Get_y() const
{
	return center.Get_y();
}

float& Ball::Get_x() 
{
	return center.Get_x();
}

float& Ball::Get_y() 
{
	return center.Get_y();
}

void CollisionBall(Ball &ballA, Ball &ballB)
{
	float dist = ballA.center.GetDistance(ballB.center);

	if (dist <= ballA.r + ballB.r)
	{
		while (dist <= ballA.r + ballB.r)
		{
			ballA.center = ballA.center.Move(ballA.speed, ballA.center, -1);
			ballB.center = ballB.center.Move(ballB.speed, ballB.center, -1);
			dist = ballA.center.GetDistance(ballB.center);
		}
		ballA.speed = ballA.speed.Reflection(ballB.center, ballA.center);
		ballB.speed = ballB.speed.Reflection(ballA.center, ballB.center);
	}
}

Line::Line(float x, float y) : start(x, y), pen(gdi::Color::Gray, 2)
{}

void Line::Draw(gdi::Graphics &canvas, Field &field) const
{
	POINT p;
	GetCursorPos(&p);
	Vector_2D Cursor(static_cast<float>(p.x), static_cast<float>(p.y));
	float length = Cursor.GetDistance(start);
	float x = Cursor.Get_x();
	float y = Cursor.Get_y();
	for (float k = 0.0f; k < length; k++)
	{
		float new_x = start.Get_x() + k * Cos(start, Cursor);
		float new_y = start.Get_y() + k * Sin(start, Cursor);
		int i = static_cast<int>(new_x / texture_size);
		int j = static_cast<int>(new_y / texture_size);
		if (0 <= i && i < field.GetWidth() && 0 <= j && j < field.GetHeight())
		{
			if (field[i][j] || j < 2)
			{
				x = new_x;
				y = new_y;
				break;
			}
		}
	}
	canvas.DrawLine(&pen, start.Get_x(), start.Get_y(), x, y);
}