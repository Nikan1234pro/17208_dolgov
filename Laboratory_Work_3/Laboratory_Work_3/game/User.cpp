#include "User.h"
#include "../Intelligence.h"

User::User(float x, float y, gdi::Color color) : ball(x, y, color), score(0)
{}

void User::Update(float dt, Field &field, int screen_w, int screen_h)
{
	ball.Update(dt, screen_w, screen_h);
	score += ball.GetPhysics(field);
}

int User::GetScore() const
{
	return score;
}

void Update(User &A, User &B)
{
	CollisionBall(A.ball, B.ball);
}

User::~User()
{}


Player::Player(float x, float y, gdi::Color color) : User(x, y, color)
{}

void Player::Draw(gdi::Graphics &canvas, int screen_w, int screen_h, Field &field) const
{
	if (ball.OnBase())
	{
		Line line(ball.Get_x(), ball.Get_y());
		line.Draw(canvas, field);
	}
	ball.Draw(canvas);
}

void Player::ProcessInput(const bool keys[256])
{
	if (keys[32]) //SPACE
	{
		POINT p;
		if (GetCursorPos(&p))
			ball.SetSpeed(p.x, p.y);
	}
	if (keys[8])
		ball.SetCenter();
}


Bot::Bot(float x, float y, gdi::Color color) : User(x, y, color), min_dt(0)
{}

void Bot::Draw(gdi::Graphics &canvas, int screen_w, int screen_h)
{
	ball.Draw(canvas);
}

void Bot::Update(float dt, Field &field, int screen_w, int screen_h)
{
	User::Update(dt, field, screen_w, screen_h);
	if (ball.OnBase())
	{
		min_dt += dt;
		if (min_dt > 1.0f)
		{
			float x = std::rand() % 1900;
			float y = std::rand() % 1000;
			Vector_2D Position = Intelligence::GetInstance()->GetPoint(ball.Get_x(), ball.Get_y(), field, screen_w, screen_h);
			ball.SetSpeed(Position);
			min_dt = 0;
		}
	}
}

void DrawScore(gdi::Graphics &canvas, int screen_w, int screen_h, const User &A, const User &B)
{
	gdi::SolidBrush brushHead(gdi::Color(140, 40, 100, 190));
	gdi::Pen pen(gdi::Color(255, 40, 100, 138));
	gdi::RectF Head(0.0f, 0.0f, screen_w, texture_size * 2.0f);
	canvas.DrawRectangle(&pen, Head);
	canvas.FillRectangle(&brushHead, Head);

	WCHAR symbol = ':';
	gdi::Font myFont(L"Unispace", 26);
	gdi::StringFormat format;
	format.SetAlignment(gdi::StringAlignmentCenter);
	gdi::RectF Rect1(screen_w / 2.0f - texture_size, -texture_size / 4.0f, texture_size * 2.0f, texture_size * 2.0f);
	gdi::SolidBrush blackBrush(gdi::Color(255, 0, 0, 0));
	canvas.DrawString(&symbol, 1, &myFont, Rect1, &format, &blackBrush);

	std::wstring scoreA = std::to_wstring(A.GetScore());
	format.SetAlignment(gdi::StringAlignmentFar);
	gdi::RectF Rect2(0.0f, 0.0f, screen_w / 2.0f - texture_size, texture_size * 2.0f);
	gdi::SolidBrush redBrush(gdi::Color::Red);
	canvas.DrawString(scoreA.c_str(), scoreA.size(), &myFont, Rect2, &format, &redBrush);

	std::wstring scoreB = std::to_wstring(B.GetScore());
	format.SetAlignment(gdi::StringAlignmentNear);
	gdi::RectF Rect3(screen_w / 2.0f + texture_size, 0.0f, screen_w / 2.0f - texture_size, texture_size * 2.0f);
	gdi::SolidBrush blueBrush(gdi::Color::Blue);
	canvas.DrawString(scoreB.c_str(), scoreB.size(), &myFont, Rect3, &format, &blueBrush);
}





