#include "game.h"
#include "game/Textures.h"
#include <iostream>
Game::Game() : gui::BaseGameWindow(0, 0, Window_width, Window_height), 
			   arkanoid(GetWidth() / texture_size, GetHeight() / texture_size, GetWidth(), GetHeight())
{
	std::cout << GetWidth() << std::endl;
	std::cout << GetHeight() << std::endl;
}

void Game::Render(gdi::Graphics &canvas) 
{
	gdi::Image *image = Textures::GetInstance()->GetTexture("background")[0];
	canvas.DrawImage(image, 0, 0, GetWidth(), GetHeight());
	arkanoid.Draw(canvas);
}

void Game::ProcessInput(const bool keys[256]) 
{
	 arkanoid.ProcessInput(keys);
}

void Game::Update(float dt) 
{
	arkanoid.UpdateWorld(dt); 
}

Game::~Game()
{}
