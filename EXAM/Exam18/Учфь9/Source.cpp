#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <cstdlib>

class Test
{
public:
	Test()
	{
		std::cout << "created" << std::endl;
	}

	Test(Test &&other)
	{
		std::cout << "Test(&&)" << std::endl;
	}

	Test& operator=(Test &&other)
	{
		std::cout << "= (&&)" << std::endl;
		return *this;
	}

	void foo()
	{
		std::cout << "addres of object:" << this << std::endl;
	}
};

class Allocator
{
public:
	void* allocate(size_t size) const
	{
		void *ptr = malloc(size);
		return ptr;
	}

	void free(void *ptr) const
	{
		free(ptr);
	}

	template <class T>
	void instance(void *ptr)
	{
		new(ptr) T();
	}

	template <class T>
	void destroy(void *ptr)
	{
		ptr->~T();
	}
};


template <class Iter>
void advance(Iter &it, int n, std::random_access_iterator_tag)
{
	std::cout << "Random" << std::endl;
	it += n;
}

template <class Iter>
void advance(Iter &it, int n, std::bidirectional_iterator_tag)
{
	std::cout << "Bidirectional" << std::endl;
	for (int i = 0; i < n; i++)
		it++;
}

template <class Iter>
void advance(Iter &it, int n)
{
	advance(it, n, std::iterator_traits<Iter>::iterator_category());
}


int main()
{
	std::vector<int> vect;
	std::list<int> lst;
	for (int i = 0; i < 10; i++)
	{
		vect.push_back(i * 5);
		lst.push_back(i * 8);
	}
	std::vector<int>::iterator it1 = vect.begin();
	std::list<int>::iterator it2 = lst.begin();

	advance(it1, 3);
	advance(it2, 2);

	std::cout << *it1 << "  " << *it2 << std::endl;

	Allocator allocator;
	void *ptr = allocator.allocate(sizeof(Test));
	std::cout << ptr << std::endl;
	allocator.instance<Test>(ptr);
	std::cout << ptr << std::endl;

	



	std::cin.get();
	return 0;
}