#include <iostream>
#include <vector>
#include <list>

template <typename T>
class A{
public:
	A(T data){
		std::cout << "T" << std::endl;
	}
};

template <typename T>
class A<T*>{
public:
	A(T* data) {
		std::cout << "T*" << std::endl;
	}
};

template<>
class A<int>
{
public:
	A(int data) {
		std::cout << "int" << std::endl;
	}
};

void test(int&& ref) {
	ref = 4;
}

int main(){
	int a = 90;
	char c = 'j';
	A<int> v(a);
	A<int*> w(&a);
	A<char> x(c);

	int k = 0;
	test(std::move(k));
	std::cout << "ew a  =" << k << std::endl;

	std::cin.get();
	return 0;
}