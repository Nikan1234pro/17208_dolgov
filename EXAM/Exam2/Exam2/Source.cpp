#include <memory>
#include <iostream>

std::weak_ptr<int> wp;

void f()
{
	if (auto p = wp.lock())
		std::cout << *p << std::endl;
	else
		std::cout << "Shit, it died!" << std::endl;
}

int main()
{
	
	{
		std::shared_ptr<int> sp(new int);
		*sp = 5;
		wp = sp;
		f();
	}
	f();



	std::unique_ptr<int[]> ptr(new int[10]);
	for (int i = 0; i < 10; i++)
		ptr[i] = 2 * i;

	for (int i = 0; i < 10; i++)
		std::cout << ptr[i] << std::endl;

	std::shared_ptr<int> sptr(new int);
	{
		std::shared_ptr<int> p2 = sptr;
		std::cout << sptr.unique() << std::endl;
	}
	std::cout << sptr.unique() << std::endl;



	system("pause");
	return 0;
}