#pragma once

class Base{
	int data;
	int *ptr;
public:
	Base() : data(0){
		ptr = new int(12);
	}

	Base(const Base& other) : data(other.data){
		ptr = new int(*(other.ptr));
	}

	Base& operator=(const Base& other){
		if (this != &other) {
			data = other.data;
			ptr = new int(*(other.ptr));
		}
		return *this;
	}

	Base(Base&& other) : data(other.data) {
		ptr = other.ptr;
		other.ptr = nullptr;
	}

	Base& operator = (Base&& other) {
		if (this != &other) {
			data = other.data;
			delete ptr;
			ptr = other.ptr;
			other.ptr = nullptr;
		}
		return *this;
	}

	virtual ~Base(){
		delete ptr;
	}
};

class Derv : public Base {
	char *arr;
public:
	Derv() : Base() {
		arr = new char(321);
	}

	Derv(const Derv &other) : Base(other) {
		arr = new char(*(other.arr));
	}

	Derv& operator=(const Derv &other){
		if (this != &other){
			*(static_cast<Base*>(this)) = other;
			delete arr;
			arr = new char(*(other.arr));
		}
		return *this;
	}

	Derv(Derv &&other) : Base(other), arr(other.arr) {
	}

	Derv& operator=(Derv &&other){
		if (this != &other){
			*(static_cast<Base*>(this)) = other;
			delete arr;
			arr = other.arr;
			other.arr = nullptr;
		}
		return *this;
	}

	virtual ~Derv(){
		delete arr;
	}
};