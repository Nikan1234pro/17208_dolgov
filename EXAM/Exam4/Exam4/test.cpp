#include <iostream>

int sum(int a, int b)
{
	return a + b;
}

int mul(int a, int b)
{
	return a * b;
}

int foo(int(*func)(int, int), int a, int b)
{
	return func(a,b);
}

typedef int(*defined_f)(int, int);


int aux(defined_f func, int a, int b)
{
	return func(a, b);
}



void test1()
{
	
	std::cout << std::endl;
	int a = 30;
	int b = 9;
	std::cout << foo(sum, a, b) << std::endl;
	std::cout << foo(mul, a, b) << std::endl;
	int(*keks)(int, int) = &mul;
	std::cout << aux(keks, a, b) << std::endl;

	
}