#include <iostream>

class SomeClass
{
	int A;
public:
	SomeClass(int a) : A(a)
	{}

	int func(int B)
	{
		return A + B;
	}

	void foo() const
	{
		std::cout << A << std::endl;
	}
};


typedef int(SomeClass::*fpointer)(int);

void test2()
{
	SomeClass x(9);
	void (SomeClass::*FF)() const = &SomeClass::foo;
	(x.*FF)();
	
	fpointer fp = &SomeClass::func;
	std::cout << (x.*fp)(8) << std::endl;
}