#include <iostream>
#include <vector>
#include <cstdio>
#include <ctime>
#include <algorithm>
extern void test1();
extern void test2();

bool another_cmp(const int &lhs, const int &rhs)
{
	return (lhs > rhs);
}

template<class T>
class Lambda
{
	T &ref;
public:
	Lambda(T &r) : ref(r)
	{}

	void operator()(T n)
	{
		if (ref < n)
			ref = n;
	}
};

int main()
{

	std::srand(std::time(NULL));
	std::vector<int> vect;
	for (int i = 0; i < 10; i++)
	{
		int tmp = std::rand() % 1000;
		std::cout << tmp << ' ';
		vect.push_back(tmp);
	}
	std::cout << std::endl;

	int max1 = 0;
	int max2 = 0;
	int max3 = 0;
	int max4 = 0;
	std::for_each(vect.begin(), vect.end(), [&](int n) { if (max1 < n) max1 = n;}); //find max with lambda
	std::for_each(vect.begin(), vect.end(), Lambda<int>(max2));						//find max with Functor
	std::cout << max1 << std::endl;
	std::cout << max2 << std::endl;

	auto lambda = [&max3](int n) noexcept ->void { if (max3 < n) max3 = n; };
	auto *printer = new auto([](int n) noexcept ->void { std::cout << n << ' '; });

	std::for_each(vect.begin(), vect.end(), lambda);								//find max with lambda-function object
	std::cout << max3 << std::endl;

	std::for_each(vect.begin(), vect.end(), *printer);								//print vector with lambda pointer


	auto cmp = [](const int &lhs, const int &rhs) ->bool { return (lhs < rhs); };  //sort with lambda
	std::sort(vect.begin(), vect.end(), cmp);

	std::cout << std::endl;
	std::for_each(vect.begin() ,vect.end(), *printer);			//print vector with lambda pointer

	bool(*p_cmp)(const int&, const int&) = &another_cmp;		//function pointer
	std::sort(vect.begin(), vect.end(), p_cmp);					//sort with function pointer
	for (auto x : vect)
		std::cout << x << ' ';
	std::cout << std::endl;


	test1();
	test2();

	system("pause");
	return 0;
}