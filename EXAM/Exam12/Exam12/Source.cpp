#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include <iomanip>

class String
{
	char* data;
public:
	String() : data(nullptr)
	{
		std::cout  << "String()	" << "data = " << (void*)data << std::endl;
	}

	String(const char *str)
	{
		data = new char[strlen(str) + 1];
		strcpy(data, str);
		std::cout << "String(char*)	" << "data = " << (void*)data << std::endl;
	}

	String(const String &other)
	{
		data = new char[strlen(other.data) + 1];
		strcpy(data, other.data);
		std::cout << "String(String &)	" << "data = " << (void*)data << std::endl;
	}

	String(String &&other)
	{
		data = other.data;
		other.data = nullptr;
		std::cout << "String(String &&)	" << "data = " << (void*)data << std::endl;
	}

	String& operator=(const String &other)
	{
		if (this != &other)
		{
			delete[] data;
			data = new char[strlen(other.data) + 1];
			strcpy(data, other.data);
		}
		std::cout << "operator=(String &)	" << "data = " << (void*)data << std::endl;
		return *this;
	}

	String& operator=(String &&other)
	{
		if (this != &other)
		{
			delete[] data;
			data = other.data;
			other.data = nullptr;
		}
		std::cout << "operator=(String &&)	" << "data = " << (void*)data << std::endl;
		return *this;
	}

	friend String operator+(const String &s1, const String &s2)
	{
		String tmp;
		tmp.data = new char[strlen(s1.data) + strlen(s2.data) + 1];
		strcpy(tmp.data, s1.data);
		strcpy(tmp.data, s2.data);
		std::cout << "operator+()	 res = " << (void*)tmp.data << std::endl;
		return tmp;
	}

	friend std::ostream& operator << (std::ostream &os, const String &str)
	{
		if (str.data == nullptr)
		{
			std::cout << "Output error!" << std::endl;
			return os;
		}
		os << str.data;
		return os;
	}

	virtual ~String()
	{
		delete[] data;
	}

};

int main()
{
	String S1("Hello");
	String S2(S1);
	String S3(std::move(S2));
	std::cout << S3 << std::endl;
	std::cout << S2 << std::endl;

	String SS = S1 + S3;



	std::cin.get();
	return 0;
}