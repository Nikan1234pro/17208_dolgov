#include <iostream>
#include <vector>
#include <map>

using namespace std;
int main()
{
	vector<int> vect;
	for (int i = 0; i < 10; i++)
		vect.push_back(i * 6);
	std::cout << *(&vect[0] + 2) << " = 2 * 6 = 12" << std::endl;


	vect.insert(vect.begin() + 5, 45);
	std::cout << vect[5] << "	" << vect[6] << std::endl;

	int *ptr = vect.data();
	for (int i = 0; i < 10; i++)
		std::cout << ptr[i] << ' ';
	std::cout << std::endl;


	system("pause");
	return 0;
}