#include <iostream>

class Base{
public:
	~Base(){
		std::cout << "Delete Base" << std::endl;
	}
};

class Base2 {
public:
	~Base2() {
		std::cout << "Delete Base2" << std::endl;
	}
};

class Derv : public Base, public Base2{
public:
	~Derv(){
		std::cout << "Delete Derv" << std::endl;
	}
};

class EXCEPT {
	int *a;
public:
	EXCEPT() {
		try {
			a = new int[12];
		}
		catch (...) {   //����� �� ������
			delete a;
			throw;
		}
	}
};