#include <iostream>
#include "Header.h"

namespace exam {
	template <typename Iter>
	void advance(Iter &it, int n)
	{

	}
}

class Complex{
	double Re = 0;
	double Im = 0;
public:
	Complex()
	{}

	Complex(double re, double im) : Re(re), Im(im){
	}

	Complex(double re) : Re(re){
	}

	friend Complex operator+(const Complex &z, const Complex &w);
	friend std::ostream& operator<<(std::ostream &os, Complex &z);
};

Complex operator+(const Complex &z, const Complex &w) {
	double re = z.Re + w.Re;
	double im = z.Im + w.Im;
	return Complex(re, im);
}

std::ostream& operator<<(std::ostream &os, Complex &z)
{
	os << z.Re << '+' << z.Im << 'i';
	return os;
}

int main(){
	Complex a(5);
	Complex b(10, 6);
	Complex c = a + b;
	std::cout << c << std::endl;

	{
		Derv d;
	}

	EXCEPT ex;

	system("pause");
	return 0;
}