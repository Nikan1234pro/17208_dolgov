#include <iostream>

class SomeClass{
private:
	static int a;
	SomeClass() {
		a++;
	}
public:
	static SomeClass& GetInstance() {
		static SomeClass instance;
		return instance;
	}
	void foo(){
		std::cout << a << std::endl;
	}
};
int SomeClass::a = 0;

int main(){
	SomeClass inst = SomeClass::GetInstance();
	inst.foo();

	SomeClass inst2 = SomeClass::GetInstance();

	inst2.foo();
	system("pause");
	return 0;
}