#include <iostream>
using namespace std;
template<typename T> void foo(T) { cout << '1'; }  // 1
template<> void foo(int*) { cout << '2'; }       // 2
template<typename T> void foo(T*) { cout << '3'; }

template<typename T> void bar(T) { cout << '4'; }
template<typename T> void bar(T*) { cout << '5'; }
template<> void bar(int*) { cout << '6'; }

int main() {
	int i;
	foo(&i);
	bar(&i);
	system("pause");
	return 0;
}