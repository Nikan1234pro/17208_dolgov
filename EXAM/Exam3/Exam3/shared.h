using uint = unsigned int;

template <class T>
class shared_ptr
{
	struct data
	{
		uint count;
		T *ptr;
	};

	data *Data;
public:
	shared_ptr()
	{
		Data = new data;
		Data->count = 0;
		Data->ptr = nullptr;
	}

	shared_ptr(T *p)
	{
		Data = new data;
		Data->count = 1;
		Data->ptr = p;
	}

	shared_ptr(const shared_ptr &other)
	{
		Data = other.Data;
		Data->count++;
	}

	shared_ptr& operator=(const shared_ptr &other)
	{
		Data->count--;
		if (0 == Data->count)
		{
			delete Data->ptr;
			delete Data;
		}
		Data = other.Data;
		Data->count++;
		return *this;
	}

	~shared_ptr()
	{
		Data->count--;
		std::cout << "Delete shared_ptr" << std::endl;
		if (0 == Data->count)
		{
			std::cout << "Delete data" << std::endl;
			delete Data->ptr;
			delete Data;
		}
	}	
};