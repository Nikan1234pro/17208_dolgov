#pragma once
#include <iostream>

template <class T>
class Deleter
{
public:
	void operator()(T* ptr)
	{
		std::cout << "Deleted ptr" << std::endl;
		delete ptr;
	}
};

template <class T>
class Deleter<T[]>
{
public:
	void operator()(T* ptr_arr)
	{
		std::cout << "Deleted array" << std::endl;
		delete[] ptr_arr;
	}
};

template <typename T, typename F = Deleter<T>>
class unique_ptr
{
	T* ptr;
	F deleter;
public:
	unique_ptr() : ptr(nullptr), deleter(F{})
	{}

	unique_ptr(T *p) : ptr(p), deleter(F{})
	{}

	unique_ptr(unique_ptr &&other) noexcept : ptr(other.ptr), deleter(F{})
	{
		other.ptr = nullptr;
	}

	operator bool() const
	{
		return ptr;
	}

	unique_ptr& operator = (unique_ptr &&other)
	{
		deleter(ptr);
		ptr = other.ptr;
		other.ptr = nullptr;
		return *this;
	}

	T* get() const
	{
		return ptr;
	}

	F get_deleter() const
	{
		return F{};
	}

	T operator *() const
	{
		return *ptr;
	}

	T* operator->() const noexcept
	{
		return ptr;
	}

	~unique_ptr()
	{
		deleter(ptr);
	}
};

template <typename T, typename E>
class unique_ptr<T[], E> 
{
	T *ptr;
	E deleter;
public:
	unique_ptr(T* p) : ptr(p)
	{
		std::cout << "LOOOOOOOOL" << std::endl;
	}

	~unique_ptr()
	{
		deleter(ptr);
	}
	
};




