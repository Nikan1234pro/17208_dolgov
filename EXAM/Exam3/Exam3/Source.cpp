#include <iostream>
#include <memory>
#include "unique_ptr.h"
#include "shared.h"

struct Test
{
	int a;
	char ch;
	Test() : a(12000), ch('s')
	{}
};

int main()
{
	{
		unique_ptr<int> ptr(new int(10));
		if (ptr)
			std::cout << ptr << std::endl;

		unique_ptr<Test, Deleter<Test>> sptr(new Test{});
		std::cout << sptr->a << std::endl;

		unique_ptr<int[]> arr_ptr(new int[34]);
	}

	{
		Test *pp = new Test;
		Test *kp = new Test;

		shared_ptr<Test> my_shared(pp);
		shared_ptr<Test> my_2;
		my_2 = my_shared;
	}


	system("pause");
	return 0;
}