// MoveDemo.cpp : ���� ���� �������� ������� "main". ����� ���������� � ������������� ���������� ���������.
//
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include <string>
#include <vector>



using namespace std;

class A;
ostream& operator<<(ostream&, const A&);

class A {
	int val{ 0 };
	char *ptr{ nullptr };
public:
	A() :A(0, nullptr) {};

	A(int v, const char *p) :val(v) {
		cout << "A(" << v << ", " << (p != nullptr ? ('"' + string(p) + '"') : "nullptr") << " )" << endl;
		if (p != nullptr) {
			ptr = new char[strlen(p) + 1];
			strcpy(ptr, p);
		}
	}

	A(const A& r) : val(r.val) {
		cout << "A(" << r << ")" << endl;
		if (r.ptr != nullptr) {
			ptr = new char[strlen(r.ptr) + 1];
			strcpy(ptr, r.ptr);
		}
	}

	A& operator= (const A& r) {
		cout << " " << *this << ".operator= (" << r << ")" << endl;
		if (&r != this) {
			val = r.val;
			delete[]ptr;
			ptr = new char[strlen(r.ptr) + 1];
			strcpy(ptr, r.ptr);
		}
		return *this;
	}

	A(A&& rm) noexcept {  //noexcept is required for move semantics in containers resize operations
		cout << "A( " << rm << " && )" << endl;
		val = rm.val;
		rm.val = 0;
		ptr = rm.ptr;
		rm.ptr = nullptr;
	}

	A& operator= (A&& rm) {
		cout << " " << *this << ".operator= (" << rm << " &&)" << endl;
		if (&rm != this) {
			val = rm.val;
			rm.val = 0;
			delete[]ptr;
			ptr = rm.ptr;
			rm.ptr = nullptr;
		}
		return *this;
	}

	~A() {
		cout << *this << ".~A()" << endl;
		delete[] ptr;
		ptr = nullptr;
	}

	int getVal() const { return val; }

	const char* getStr() const { return ptr; }

	void foo() const & {
		cout << *this << ".foo() &" << endl;
	}
	void foo() const && {
		cout << *this << ".foo() &&" << endl;
	}

};

ostream& operator<<(ostream& o, const A& a) {
	o << "A{ " << a.getVal() << ", ";
	if (a.getStr() != nullptr) {
		o << '"' << a.getStr() << '"' << "(" << (void*)a.getStr() << ")";
	}
	else {
		o << "nullptr" << "(0)";
	}
	o << " }";
	return o;
}


class B {
	string s{ "abc" };  //default value for s field
	void bar() {}
	class Nested {  //nested class has access to outer members
		void do_some() {
			B().bar();
		}
	};
	const int val = 10;
public:
	B() {
		cout << "B() " << s << endl;
	}
	B(const string& str, const int v = 0) : s{ str }, val{ v } {
		cout << "B(const string&) " << s << " " << endl;
	}

	B(const B&) = default;
	B& operator=(const B&) = default;

};


int main(int argc, char **argv)
{
	std::cout << "main()" << endl;

	const char *p{ nullptr };

	std::cout << "nullptr: " << ((void*)p) << endl;

	A a{ 10, "Hello!" };



	cout << "a==" << a << endl;
	A a2(a);

	cout << "a2==" << a2 << endl;
	a2.foo();

	A a3{ 11, "Another!" };
	cout << "a3 ==" << a3 << endl;
	a = std::move(a3);
	cout << "after a=a3: a3 ==" << a3 << endl;
	cout << "a== " << a << endl;
	
	A a4 = A{ 1, "rvalue?" };
	A{ 12, "rvalue2" }.foo();

	cout << "avect0(2)" << endl;
	vector<A> avect0;
	avect0.reserve(2);

	cout << "--avect0(2)" << endl;
	avect0.push_back(a);
	avect0.push_back(A(100, "should move?"));

	cout << "avect= {a, a2}" << endl;
	vector<A> avect = { a, a2 };
	cout << "avect.reserve(avect.capacity() + 1)" << endl;
	avect.reserve(avect.capacity() + 1);

	cout << "avect2(std::move(avect)) " << endl;

	vector<A> avect2(std::move(avect));
	cout << "--avect2" << endl;
	cout << "avect.size() == " << avect.size() << endl;

	B b;
	B b2("asdasl");
	vector<int> v1(10);  //constructor with capacity is used
	cout << "v1.size()==" << v1.size() << " v1[0] == " << v1[0] << endl;
	vector<int> v2 = { 10 }; //constructor from std::initializer_list is used
	cout << "v2.size()==" << v2.size() << " v2[0] == " << v2[0] << endl;

	system("pause");
	return 0;
}